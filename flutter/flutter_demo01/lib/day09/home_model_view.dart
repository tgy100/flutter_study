
import "package:flutter/foundation.dart" show ChangeNotifier;


class HomeModelView with ChangeNotifier {

  late int _count = 0;

  int get count => _count;

  set count(int value) {
    _count = value;
    notifyListeners();
  }
}