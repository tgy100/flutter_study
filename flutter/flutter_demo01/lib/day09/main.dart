
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './home_model_view.dart';

void main(List<String> args) {

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_){
        return HomeModelView();
      })
    ],
    child: MyApp(),
  ));
}


class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    // Selector<HomeModelView,HomeModelView>(
    //   builder: (BuildContext context, HomeModelView modelView, Widget? child){
    //
    //     return FloatingActionButton(
    //       onPressed: (){
    //         modelView.count++;
    //       },
    //       child: child,
    //     );
    //   },
    //   selector: (BuildContext context,HomeModelView view){
    //
    //     return view;
    //   },
    //   child:const Icon(Icons.pets),
    // );

    // Consumer<HomeModelView>(
    //     builder: (BuildContext context, HomeModelView modelView, Widget? child) {
    //       print("floatingActionButton provider");
    //       return FloatingActionButton(onPressed: (){
    //         modelView.count++;
    //       });
    //     }
    // )

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("provider"),
        ),
        body: Center(
          child: Column(
            children: [
              OneWidget(),
              TwoWidget(),
            ],
          ),
        ),
        floatingActionButton: Selector<HomeModelView,HomeModelView>(
          builder: (BuildContext context, HomeModelView modelView, Widget? child){

            print("floatingActionButton Selector");

            return FloatingActionButton(
              onPressed: (){
                modelView.count++;
              },
              child: child,
            );
          },
          shouldRebuild: (pre,next)=>false,
          selector: (BuildContext context,HomeModelView view){

            return view;
          },
          child:const Icon(Icons.pets),
        ),
      ),
    );
  }
}


class OneWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    // int count = Provider.of<HomeModelView>(context).count;
    print("OneWidget provider");


    // return Container(
    //   width: 200,

    //   height: 40,
    //   color: Colors.blue,
    //   child: Text(
    //     "one widget $count",
    //     style: const TextStyle(
    //       fontSize: 20,
    //       color: Colors.white,
    //     ),),
    // );

    return Container(
      width: 200,
      height: 40,
      color: Colors.blue,
      child: Consumer<HomeModelView>(
        builder: (BuildContext context,HomeModelView modelView,Widget? widget){

          return Text(
            "one widget ${modelView.count}",
            style: const TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),);
        },
      ),
    );
  }

}


class TwoWidget extends StatefulWidget{

  @override
  State<TwoWidget> createState() => _TwoWidgetState();
}

class _TwoWidgetState extends State<TwoWidget> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // final HomeModelView modelView = Provider.of<HomeModelView>(context);

    print("_TwoWidgetState provider");


    return Container(
      width: 200,
      height: 40,
      color: Colors.purple,
      child: Consumer<HomeModelView>(
        builder: (BuildContext context, HomeModelView modelView, Widget? child){
          return Text(
            "two widget ${modelView.count}",
            style: const TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),);
        },
      ),
    );

    // return Container(
    //   width: 200,
    //   height: 40,
    //   color: Colors.purple,
    //   child: Text(
    //     "one widget ${modelView.count}",
    //     style: const TextStyle(
    //       fontSize: 20,
    //       color: Colors.white,
    //     ),),
    // );
  }
}
