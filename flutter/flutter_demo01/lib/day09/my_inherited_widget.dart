import 'package:flutter/material.dart';

class MyInheritedWidget extends InheritedWidget {

  final int count;

  const MyInheritedWidget({Key? key,required this.count, required Widget child}) : super(key: key, child: child);

  static MyInheritedWidget of(BuildContext context) {

    return context.dependOnInheritedWidgetOfExactType()!;
  }

  @override
  bool updateShouldNotify(covariant MyInheritedWidget oldWidget) {
    // TODO: implement updateShouldNotify
    // return oldWidget.count != count;
    // 返回true，则会调用使用了
    return true;
  }

}


class MyApp extends StatefulWidget {

  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  int count = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("inheritWidget"),
        ),
        body: Center(
          child: MyInheritedWidget(
            count: count,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                OneWidget(),
                TwoWidget(),
                ThreeWidget(),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              count++;
            });
          },
          child: const Icon(Icons.pets),
        ),
      ),
    );
  }
}


class OneWidget extends StatelessWidget {

  const OneWidget({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    int count = MyInheritedWidget.of(context).count;

    return Container(
      width: 200,
      height: 40,
      color: Colors.blueAccent,
      child: Text("one:$count",style: const TextStyle(color: Colors.white,fontSize: 20),),
    );
  }
}

class TwoWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TwoWidgetState();
  }
}

class _TwoWidgetState extends State<TwoWidget> {

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

    print("_TwoWidgetState");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    int count = MyInheritedWidget.of(context).count;

    return Container(
      width: 200,
      height: 40,
      color: Colors.blueAccent,
      child: Text("two:$count",style: const TextStyle(color: Colors.white,fontSize: 20),),
    );
  }
}

class ThreeWidget extends StatefulWidget {

  @override
  State<ThreeWidget> createState() => _ThreeWidgetState();
}

class _ThreeWidgetState extends State<ThreeWidget> {

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

    print("_ThreeWidgetState");
  }

  @override
  Widget build(BuildContext context) {

    // MyInheritedWidget.of(context);

    return Container(
      width: 200,
      height: 40,
      color: Colors.yellow,
    );
  }
}