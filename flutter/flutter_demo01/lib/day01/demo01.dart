import 'package:flutter/material.dart';

void main() {

  // runApp(Center(
  //     child: Text(
  //       "hello world",
  //       textDirection: TextDirection.ltr,
  //       style: TextStyle(fontSize: 20),
  //     )
  // )
  // );
  // runApp(
  //     MaterialApp(
  //       home: Scaffold(
  //         appBar: AppBar(title: Text("tgy")),
  //         body: Center(
  //           child: Text(
  //             "hello world",
  //             style: TextStyle(fontSize: 20),),
  //         ),
  //       ),
  //     )
  // );

  // runApp(
  //     MaterialApp(
  //       home: Scaffold(
  //         appBar: AppBar(
  //             title: Text("flutter")
  //         ),
  //         body: Center(
  //           child: Row(
  //             mainAxisAlignment: MainAxisAlignment.center,
  //             children: [
  //               Checkbox(value: true, onChanged: (val){}),
  //               Text(
  //                   "hello world",
  //                   style: TextStyle(color: Colors.red)
  //               )
  //             ],
  //           ),
  //         ),
  //       ),
  //     )
  // );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: MyScaffold(),
    );
  }
}

class MyScaffold extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
        appBar: AppBar(
            title: Text("flutter")
        ),
        body: BodyFulWidget()
    );
  }
}

class BodyWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Checkbox(value: true, onChanged: (val){

          }),
          Text(
              "同意协议",
              style: TextStyle(color: Colors.red))
        ],
      ),
    );
  }
}

class BodyFulWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MyFulStat();
  }
}

class MyFulStat extends State<StatefulWidget> {

  bool checkState = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

          Checkbox(value: checkState, onChanged: (val){

            print(val);
            setState(() {
              checkState = val!;
            });
          }),
          Text("请同意协议",style: TextStyle(color: Colors.red))
        ],
      ),
    );
  }


}


