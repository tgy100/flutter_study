import 'package:flutter/material.dart';


void main() {

  runApp(MyApp());
}


class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("demo04-statefulWidget"),),
        body: BodyfulWidget(),
      ),
    );
  }
}


class BodyfulWidget extends StatefulWidget {
  const BodyfulWidget({Key? key}) : super(key: key);

  @override
  _BodyfulWidgetState createState() => _BodyfulWidgetState(count: 10);
}

class _BodyfulWidgetState extends State<BodyfulWidget> {

  int count;

  _BodyfulWidgetState({this.count = 0});

  @override
  Widget build(BuildContext context) {
    return Center(
          child: Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              border: Border.all(
                width: 2,
                color: Colors.green
              )
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(onPressed: (){

                      setState(() {

                        count++;
                      });
                    }, child: Text("+1")),
                    ElevatedButton(onPressed: (){

                      setState(() {
                        if(count > 0) {
                          count--;
                        }
                      });
                    }, child: Text("-1"))
                  ],
                ),
                Text("当前计数${count}")
              ],
            ),
          ),

    );
  }
}



