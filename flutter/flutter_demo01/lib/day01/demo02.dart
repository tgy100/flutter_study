import 'package:flutter/material.dart';

void main(List<String> args) {

  runApp(MyApp());
}


class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("首页",style: TextStyle(fontSize: 20,color: Colors.red)),
        ),
        body: BodyFulWidget("同意协议"),
      ),
    );

  }
}

class BodyWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Checkbox(value: true, onChanged: (val) {

          }),
          Text("hello world")
        ],
      ),
    );
  }
}

class BodyFulWidget extends StatefulWidget {

  final String  title;

  BodyFulWidget(this.title);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BodyFullState();
  }

  void say() {

  }
}

class _BodyFullState extends State<BodyFulWidget> {

  bool flag = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Checkbox(value: flag, onChanged: (val) {

            setState(() {

              flag = val!;
            });
          }),
          Text(widget.title ?? "",style: TextStyle(fontSize: 24,color: Colors.red))
        ],
      ),
    );
  }
}