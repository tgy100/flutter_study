import 'package:flutter/material.dart';


void main() {

  runApp(MyApp());
}


class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(
          primaryColor: Colors.deepOrangeAccent
      ),
      home: Scaffold(
        appBar: AppBar(title: Text("demo03"),),
        body: BodyWidget(),
      ),
    );
  }
}

class BodyWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: [
        ItemWidget("黄金香 中华老字号 八分瘦广式腊肠600g", "108.8", "https://img13.360buyimg.com/mobilecms/s372x372_jfs/t1/170554/31/24601/166592/618dddaaE75c98ad7/ad5a8e2c4e7f8ea5.jpg"),
        ItemWidget("黄金香八分瘦广式腊肠600g", "108.8", "https://img13.360buyimg.com/mobilecms/s372x372_jfs/t1/170554/31/24601/166592/618dddaaE75c98ad7/ad5a8e2c4e7f8ea5.jpg"),
        ItemWidget("黄金香 中华老字号", "108.8", "https://img13.360buyimg.com/mobilecms/s372x372_jfs/t1/170554/31/24601/166592/618dddaaE75c98ad7/ad5a8e2c4e7f8ea5.jpg"),
        ItemWidget("黄金香 中华老字号 八分瘦广式腊肠600g", "108.8", "https://img13.360buyimg.com/mobilecms/s372x372_jfs/t1/170554/31/24601/166592/618dddaaE75c98ad7/ad5a8e2c4e7f8ea5.jpg"),
        ItemWidget("黄金香 中华老字号 八分瘦广式腊肠600g", "108.8", "https://img13.360buyimg.com/mobilecms/s372x372_jfs/t1/170554/31/24601/166592/618dddaaE75c98ad7/ad5a8e2c4e7f8ea5.jpg"),
      ],
    );
  }
}

class ItemWidget extends StatelessWidget {

  final String title;
  final String price;
  final String imageUrl;

  ItemWidget(this.title,this.price,this.imageUrl);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border.all(
              width: 5,
              color: Colors.green
          )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title),
          SizedBox(height: 10,),
          Text(price,style: TextStyle(fontSize: 18,color: Colors.red),),
          SizedBox(height: 10,),
          Image.network(imageUrl)
        ],
      ),
    );
  }
}