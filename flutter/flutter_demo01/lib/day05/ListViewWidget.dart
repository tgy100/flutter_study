import 'dart:math';
import 'package:flutter/material.dart';


class ListViewWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("哈哈哈"),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("呵呵"),
        ),
      ],
    );
  }
}

class ListViewWidget01 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: [
        ListTile(
          leading: Icon(Icons.pets),
          title: Text("张三"),
          subtitle: Text("13871257111"),
          trailing: Icon(Icons.delete),
        ),
        ListTile(
          leading: Icon(Icons.star),
          title: Text("李四"),
          subtitle: Text("17762341111"),
          trailing: Icon(Icons.delete),
        )
      ],
    );
  }
}

class ListViewWidget02 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 300,
      child: ListView(
        scrollDirection: Axis.horizontal,
        itemExtent: 200,
        children: [
          Container(
            // width: 200,
            color: Colors.yellow,
          ),
          Container(
            // width: 150,
            color: Colors.blue,
          ),
          Container(
            // width: 300,
            color: Colors.blueGrey,
          )
        ],
      ),
    );
  }
}

class ListViewWidget03 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.builder(
      itemCount: 20,
      itemExtent: 100,
      itemBuilder: (BuildContext context, int index){
        return Container(
          color: Color.fromARGB(Random().nextInt(255), Random().nextInt(255), Random().nextInt(255), 1),
          child: Text(
            "index ${index + 1}",
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
          ),
        );
      },

    );
  }
}

class ListViewWidget04 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.builder(
      itemCount: 20,
      itemExtent: 80,
      itemBuilder: (context,index) {
        return ListTile(
          leading: Icon(Icons.pets),
          title: Text("标题${index + 1}"),
          subtitle: Text("13871111111"),
          trailing: Icon(Icons.delete),
        );
      },

    );
  }
}

class ListViewWidget05 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.separated(
        itemBuilder: (context,index) {
          return ListTile(
            leading: Icon(Icons.pets),
            title: Text("标题${index + 1}"),
            subtitle: Text("13871111111"),
            trailing: Icon(Icons.delete),
          );
        },
        separatorBuilder: (context,index) {
          return Divider(
            indent: 10,
            thickness: 1,
            height: 5,
            color: Colors.grey,
            endIndent: 10,
          );
        },
        itemCount: 20);
  }

}