import 'package:flutter/material.dart';
import 'dart:math';

class GridViewWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GridView(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
      children: generalWidget(20),
    );
  }

  List<Widget> generalWidget(int count) {

    final widgets = <Widget>[];
    for(int i = 0; i < count;i++) {
      widgets.add(
        Container(
            width: 100 + Random().nextDouble() * 200,
            height: 100 + Random().nextDouble() * 200,
            color: Color.fromARGB(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255))
        ),
      );
    }

    return widgets;
  }
}