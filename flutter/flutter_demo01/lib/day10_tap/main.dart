import 'package:flutter/material.dart';


void main(List<String> args) {

  runApp(MyApp());
}


class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("tap"),
        ),
        body: HomeWidget(),
      ),
    );
  }

}


class HomeWidget extends StatelessWidget {

  const HomeWidget({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build


    return Listener(
      onPointerDown: (event) {

        print("按下:${event.position},${event.localPosition}");

      },
      onPointerUp: (event) {

        print("收起");
      },
      onPointerHover: (event) {
        print("hover");
      },
      onPointerMove: (event) {

        // print("移动:${event.position},${event.localPosition}");
      },
      child: Center(
        child: Container(
          width: 200,
          height: 200,
          color: Colors.yellow,
        ),
      ),
    );
  }

}
