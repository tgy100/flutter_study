import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("基础组件练习"),
        ),
        body: ConstraintWidget01(),
      ),
    );
  }
}

class CommonWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.purple,
      height: 120,
      constraints: BoxConstraints(
        minWidth: double.infinity,
        minHeight: 50,
      ),
      child: Text("xxx",style: TextStyle(color: Colors.white),),
    );
  }
}

class ConstraintWidget01 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ConstrainedBox(
      constraints: BoxConstraints(
          minWidth:60,
          minHeight: 100),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minWidth: 70,
          minHeight: 90,
        ),
        child: Container(
          width: 40,
          height: 40,
          color: Colors.red,
        ),
      ),
    );
  }

}



