

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TGYDottedLineWidget extends StatelessWidget {

  final int lineCount;
  final double lineWidth;
  final double lineHeight;
  final Color lineColor;
  final Axis direction;

  TGYDottedLineWidget({
   this.lineWidth = 5,
   this.lineCount = 10,
    this.lineHeight = 1,
   this.lineColor = const Color(0xffcccccc),
    this.direction = Axis.horizontal,
  });


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Flex(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        direction: direction,
        children: buildDottedLines(),
    );
  }

  List<Widget> buildDottedLines() {

     return List.generate(this.lineCount, (index){
       return Container(
         width: lineWidth,
         height: lineHeight,
         color: lineColor,
       );
     }).toList();

  }
}