import 'package:flutter/material.dart';


class TGYStarWidget extends StatefulWidget {

  final Axis direction;
  final double ratio;
  final int count;
  final double size;
  final Color normalColor;
  final Color highColor;
  final Widget normalWidget;
  final Widget highWidget;

  TGYStarWidget({
    Key? key,
    required this.ratio,
    this.direction = Axis.horizontal,
    this.size = 30,
    this.count = 5,
    this.normalColor = Colors.grey,
    this.highColor = Colors.red,
    Widget? normalWidget,
    Widget? highWidget,
  }):normalWidget = normalWidget ?? Icon(Icons.star,color: normalColor,size: size,),
    highWidget = highWidget?? Icon(Icons.star, color: highColor,size: size,),
    assert(ratio >= 0 && ratio <= 1,"ratio 取值范围 [0,1]"),
    super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TGYStarState();
  }
}

class _TGYStarState extends State<TGYStarWidget> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    
    return Stack(
      children: [
        Flex(
          mainAxisSize: MainAxisSize.min,
          direction: widget.direction,
          children: buildNormalStarWidgets(),
        ),
        Flex(
          mainAxisSize: MainAxisSize.min,
          direction: widget.direction,
          children: buildHighStarWidgets(),
        )
      ],
    );
  }

  List<Widget> buildNormalStarWidgets() {

    final List<Widget> widgets = List.generate(widget.count, (_) {

      return widget.normalWidget;
    }).toList();

    return widgets;
  }

  List<Widget> buildHighStarWidgets() {

    final List<Widget> widgets = [];

    double allStar =  widget.count * widget.ratio;
    int wholeStar = allStar.toInt();

    for(int i = 0;i < wholeStar; i++) {

      widgets.add(widget.highWidget);
    }

    double drawWidth = (allStar - wholeStar) * widget.size;

    final clipRectWidget = ClipRect(
      clipper: TGYClipRRect(drawWidth),
      child: widget.highWidget,
    );

    widgets.add(clipRectWidget);
    return widgets;
  }
}

class TGYClipRRect extends CustomClipper<Rect> {

  final double drawWidth;

  TGYClipRRect(this.drawWidth);

  @override
  Rect getClip(Size size) {
    // TODO: implement getClip
    return Rect.fromLTWH(0, 0, drawWidth, size.height);
  }

  @override
  bool shouldReclip(covariant TGYClipRRect oldClipper) {
    // TODO: implement shouldReclip
    return oldClipper.drawWidth != drawWidth;
  }

}



