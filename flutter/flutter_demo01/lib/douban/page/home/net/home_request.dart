import '../model/home_movie_item.dart';
import '../../net/http_requst.dart';

class TGYHomeRequest {

  static Future requestMovieItems({int page = 0,int pageSize = 25}) async{

    // HttpRequest.request(url: url)
    //type=Imdb&skip=0&limit=25&lang=Cn
    Map<String,dynamic> parameters = {
      "type":"Imdb",
      "skip":"${page * pageSize}",
      "limit":"${pageSize}",
      "lang":"Cn"
    };

    dynamic responseDatas = await TGYHttpRequest.request(
       url: "/v1/top",
      parameters:parameters
    );

    if (responseDatas is List) {

      List<TGYMovieItem> movieItems = responseDatas.map((e) {
        return TGYMovieItem.fromMap(e);
      }).toList();
      return movieItems;
    }else {
      return null;
    }

  }

}