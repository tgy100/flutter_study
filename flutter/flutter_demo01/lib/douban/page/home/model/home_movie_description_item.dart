/**
 * {
    "createdAt": 1605355459692,
    "updatedAt": 1605355459692,
    "id": "5f968bfcee3680299115bbe6",
    "poster": "https://image.querydata.org/movie/poster/1603701754760-c50d8a.jpg",
    "name": "肖申克的救赎",
    "genre": "犯罪/剧情",
    "description": "20世纪40年代末，小有成就的青年银行家安迪（蒂姆·罗宾斯 Tim Robbins 饰）因涉嫌杀害妻子及她的情人而锒铛入狱。在这座名为鲨堡的监狱内，希望似乎虚无缥缈，终身监禁的惩罚无疑注定了安迪接下来...",
    "language": "英语",
    "country": "美国",
    "lang": "Cn",
    "shareImage": "https://image.querydata.org/movie/poster/1605355459683-5f968bfaee3680299115bb97.png",
    "movie": "5f968bfaee3680299115bb97"
    }
 */
class TGYMovieDescriptionItem {

  late int createdAt;
  late String id;
  late String poster;
  late String name;
  late String genre;
  late String description;
  late String language;
  late String country;
  late String lang;
  late String shareImage;

  TGYMovieDescriptionItem.fromMap(Map<String,dynamic> map) {

    createdAt = map["createdAt"];
    id = map['id'];
    poster = map['poster'];
    name = map['name'];
    genre = map['genre'];
    description = map['description'];
    language = map['language'];
    country = map['country'];
    lang = map['lang'];
  }

}