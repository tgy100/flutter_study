import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import './model/home_movie_item.dart';
import '../custom_widget/tgy_star_widget.dart';
import '../custom_widget/tgy_dotted_line_widget.dart';

class TGYMovieItemWidget extends StatelessWidget {
  final TGYMovieItem movieItem;

  TGYMovieItemWidget(this.movieItem);

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Container(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      decoration: const BoxDecoration( border: Border(bottom: BorderSide(width: 10, color: Color(0xffe2e2e2)))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          buildRankWidget(),
          const SizedBox(height: 10,),
          buildCenterDetailWidget(),
          const SizedBox(height: 10,),
          buildDescriptionWidget(),
        ],
      ),
    );
  }


  Widget buildRankWidget() {

    return Container(

      padding: const EdgeInsets.only(left: 10,right: 10,top: 5,bottom: 5),
      decoration: const BoxDecoration(
        color: Colors.yellow,
      ),
      child: Text("NO${movieItem.index}",style: TextStyle(fontSize: 14),),
    );
  }

  Widget buildCenterDetailWidget() {

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:[
        buildImageWidget(),
        const SizedBox(width: 5,),
        Expanded(child: buildContentCenter()),
        buildRightWidget(),
      ],
    );
  }

  Widget buildImageWidget() {

    return Container(
      width: 100,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Color(0xffcccccc),
      ),
      child: AspectRatio(
        aspectRatio: 3/4,
        child: Image.network(movieItem.descriptions[0].poster,fit: BoxFit.fill,),
      ),
    );
  }

  Widget buildContentCenter() {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        buildCenterTitle(),
        buildStarWidget(),
        const SizedBox(height: 5,),
        buildDetailDescription(),
      ],
    );
  }

  Widget buildStarWidget() {

    return Row(
      children: [
        TGYStarWidget(ratio: movieItem.imdbRating / 10,size: 20,highColor: Colors.yellow,),
        Text(
          "${movieItem.imdbRating}",
          style: const TextStyle(
            fontWeight:FontWeight.w700,
          ),
        ),
      ],
    );
  }

  Widget buildDetailDescription() {

    return Text(
      "${movieItem.descriptions[0].genre} ${movieItem.descriptions[0].language} ${movieItem.descriptions[0].description}",
      style: const TextStyle(

      ),
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
    );
  }

  Widget buildCenterTitle() {

    return Wrap(
      children: [
        const Icon(
          Icons.play_circle,
          size: 25,
          color: Colors.red,
        ),
        Text(
          "${movieItem.descriptions[0].name}",
          style: const TextStyle(
            color: Colors.black,
            fontSize: 18
          ),
        ),
        Text(
          "(${movieItem.year})",
          style: const TextStyle(
            color: Colors.grey,
            fontSize: 16
          ),
        )
      ],
    );
  }

  Widget buildRightWidget() {

    return Row(
      children: [
        Container(
          width: 10,
          height: 100,
          child: TGYDottedLineWidget(
            lineWidth: 1,
            lineHeight: 5,
            lineColor: Colors.deepOrange,
            direction: Axis.vertical,

          ),
        ),
        Container(
          width: 40,
          height: 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.pets,color: Colors.yellow,),
              SizedBox(height: 5,),
              Text("想看",style: TextStyle(
                color: Colors.yellow,
              ),)
            ],
          ),
        )
      ],
    );
  }

  Widget buildDescriptionWidget() {

    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: const Color(0xffcccccc),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Text(
        "${movieItem.descriptions[0].description}",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: const TextStyle(
          color: Color(0xff777777),
          fontSize: 14,
        ),
      ),
    );
  }

}
