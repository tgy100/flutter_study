import 'package:flutter/material.dart';
import 'package:flutter_demo01/douban/page/home/model/home_movie_item.dart';
import './net/home_request.dart';
import './home_movie_item.dart';
import '../custom_widget/tgy_star_widget.dart';

class TGYHomeContent extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TGYHomeContentState();
  }
}

class _TGYHomeContentState extends State<TGYHomeContent> {

  List<TGYMovieItem> movieItems = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

     TGYHomeRequest.requestMovieItems().then((value) {

       List<TGYMovieItem>? requestMovieItems = value;
       if (requestMovieItems != null) {

         setState(() {
           movieItems.addAll(requestMovieItems);
         });
       }else {
         print("数据请求失败");
       }
    });

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(
        title: Text("首页"),
      ),
      body: buildBodyWidget(),
    );
  }

  Widget buildBodyWidget() {

    // return Center(
    //   child: TGYStarWidget(
    //     ratio:0.5,
    //     size: 20,
    //     direction: Axis.vertical,
    //   ),
    // );
    return ListView.builder(
        itemCount: movieItems.length,
        itemBuilder: (context,index) {

          return TGYMovieItemWidget(movieItems[index]);
        });
  }


}