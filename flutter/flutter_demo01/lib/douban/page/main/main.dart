import 'package:flutter/material.dart';
import '../home/home.dart';
import '../subject/subject.dart';

class TGYMainPage extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TGYMainPageState();
  }
}

class _TGYMainPageState extends State<TGYMainPage> {

  int _currentSelectIndex = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: IndexedStack(
        index: _currentSelectIndex,
        children: [
          TGYHomePage(),
          TGYSubjectPage(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        // backgroundColor: Colors.red,

        selectedFontSize: 12,
        unselectedFontSize: 12,
        selectedItemColor: Colors.green,
        unselectedItemColor: Colors.grey,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon:Icon(Icons.pets,color: Colors.grey,size: 25,),
            activeIcon: Icon(Icons.pets,color: Colors.green,size: 25,),
            label: "首页",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.delete,color: Colors.grey,size: 25,),
            activeIcon: Icon(Icons.delete,color: Colors.green,size: 25,),
            label: "我的"
          )
        ],
        currentIndex: _currentSelectIndex,
        onTap: (index) {
          setState(() {
            _currentSelectIndex = index;
          });
        },
      ),
    );
  }
}
