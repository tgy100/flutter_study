import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import './http_config.dart';

class TGYHttpRequest {
  static final Dio _dio = Dio(BaseOptions(
    baseUrl: TGYHttpConfig.baseURL,
    connectTimeout: TGYHttpConfig.timeOut,
  ));

  static Future request({
    required String url,
    String method = 'get',
    Map<String, dynamic>? parameters,
    Interceptor? interceptor,
  }) async{


    final options = Options(
      method: method,
    );

   final innerInter = InterceptorsWrapper(
      onRequest: (RequestOptions options, RequestInterceptorHandler handler) {

        handler.next(options);
      },
      onResponse: (Response e, ResponseInterceptorHandler handler) {

        handler.next(e);
      },
      onError: ( DioError e, ErrorInterceptorHandler handler){

        handler.next(e);
      }
    );

    _dio.interceptors.add(innerInter);

    if (interceptor != null) {

      _dio.interceptors.add(interceptor);
    }

    try {

      final respone = await _dio.request(url, queryParameters: parameters, options: options);
      return respone.data;
    }on Exception catch(e) {

      return Future.value(e);
    }

  }
}
