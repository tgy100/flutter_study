
import 'package:flutter/material.dart';
import './page/main/main.dart';
void main(List<String> argv) {
  
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "豆瓣",
      theme: ThemeData(
        primaryColor: Colors.blue,
        splashColor: Colors.transparent, // 点击时的高亮效果设置为透明
        highlightColor: Colors.transparent, // 长按时的扩散效果设置为透明
      ),
      home: TGYMainPage(),
    );
  }
}