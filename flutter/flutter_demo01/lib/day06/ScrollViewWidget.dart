import 'package:flutter/material.dart';

class ScrollViewWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ScrollViewState();
  }
}

class ScrollViewState extends State<ScrollViewWidget> {
  late ScrollController _scrollController;

  bool _isShowGotoTop = false;

  @override
  void initState() {
    // TODO: implement initState

    _scrollController = ScrollController();

    _scrollController.addListener(() {

      print("偏移量:${_scrollController.offset}");

      // if (_scrollController.offset > 100 ) {
      //
      //   setState(() {
      //
      //     _isShowGotoTop = true;
      //   });
      // }
      //
      // if (_scrollController.offset <= 100 ){
      //
      //   setState(() {
      //
      //     _isShowGotoTop = false;
      //   });
      // }

    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  NotificationListener(
      child: Stack(
        children: showWidgets(),
      ),
      onNotification: (ScrollNotification scrollNotification){

        double offSize = scrollNotification.metrics.pixels;

        double allheight = scrollNotification.metrics.maxScrollExtent;

        print("NotificationListener - 总高度:${allheight},当前滚动位置${offSize}");

        if (offSize > 100) {

          setState(() {
            _isShowGotoTop = true;
          });
        }else {

          setState(() {
            _isShowGotoTop = false;
          });
        }


        return false;
      },
    );
  }

  List<Widget> showWidgets() {

    final widgets = <Widget>[
      ListView.builder(
        controller: _scrollController,
        itemBuilder: (context, index) {
          return ListTile(
            leading: Icon(Icons.pets),
            title: Text("张三 ${index}"),
            subtitle: Text("123234234${index}"),
            trailing: Icon(Icons.delete),
          );
        },
        itemCount: 20,
      ),
    ];

    if (_isShowGotoTop) {

      widgets.add(Positioned(
        child: TextButton(
            onPressed: () {

              _scrollController.animateTo(0, duration: Duration(seconds: 1), curve: Curves.bounceInOut);

            },
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Colors.grey,
              ),
              width: 60,
              height: 60,
              child: Center(
                child: Text("回到顶部",style: TextStyle(color: Colors.white),),
              ),
            )),
        right: 10,
        bottom: 10,
      ));
    }

    return widgets;
  }
}
