import 'package:flutter/material.dart';

class DottedLineWidget extends StatelessWidget {

  final Axis direction;
  final int lineCount;
  final double lineWidth;
  final double lineHeight;
  final Color lineColor;

  DottedLineWidget({
    this.direction = Axis.horizontal,
    this.lineCount = 10,
    this.lineWidth = 5,
    this.lineHeight = 1,
    this.lineColor = const Color(0xffaaaaaa),
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Flex(
      direction: direction,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: List.generate(lineCount, (_){
        return SizedBox(
            width: lineWidth,
            height: lineHeight,
            child: DecoratedBox(
              decoration: BoxDecoration(
                color: lineColor,
              ),
            )
        );
      }),
    );
  }
}