import 'package:flutter/material.dart';

class SliverSafeAreaWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return CustomScrollView(
      slivers: [
        SliverSafeArea(
            sliver: SliverPadding(
              padding: EdgeInsets.all(8),
              sliver: SliverGrid(
                  delegate:SliverChildBuilderDelegate(
                        (context,index) {
                      return Container(
                        height: 200,
                        color: index % 2 == 0? Colors.blue: Colors.purple,
                      );
                    },
                    childCount: 20,
                  ),
                  gridDelegate:SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 100,
                    mainAxisSpacing: 90,
                  )
              ),
            )
        ),
      ],
    );
  }
}
