import 'package:flutter/material.dart';

class TGYStarWidget extends StatefulWidget{

  final int starCount;
  final double size;
  final double drawRatio;
  final Color highColor;
  final Color normalColor;
  final Widget highWidget;
  final Widget normalWidget;

  TGYStarWidget({
    required this.drawRatio,
    this.starCount = 5,
    this.size = 30,
    this.normalColor = Colors.grey,
    this.highColor = Colors.red,
    Widget? highWidget,
    Widget? normalWidget,
  }):this.normalWidget = normalWidget ?? Icon(Icons.star,size: size,color: normalColor,),
        this.highWidget = highWidget ?? Icon(Icons.star,size: size,color: highColor,),
        assert(drawRatio >= 0 && drawRatio <= 1, "${drawRatio} must between 0 and 1");

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TGYStarState();
  }
}

class _TGYStarState extends State<TGYStarWidget> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: generalNormalWidget(),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: generalHighWidget(),
        ),
      ],
    );
  }

  List<Widget> generalNormalWidget() {

    List<Widget> widgets = [];

    final normalWidget = widget.normalWidget;

    for(int i = 0; i < widget.starCount;i++) {
      widgets.add(normalWidget);
    }

    return widgets;
  }

  List<Widget> generalHighWidget() {

    List<Widget> widgets = [];

    int drawWholeNum = (widget.starCount * widget.drawRatio).toInt();

    final highWidget  = widget.highWidget;

    for(int i = 0; i < drawWholeNum;i++) {
      widgets.add(highWidget);
    }

    double drawWidth = (widget.starCount * widget.drawRatio - drawWholeNum) * widget.size;

    widgets.add(generalClipWidget(drawWidth));

    return widgets;
  }

  Widget generalClipWidget(double drawWidth) {

    return ClipRect(
      clipper: StarClipper(drawWidth),
      child: widget.highWidget,
    );
  }

}


class StarClipper extends CustomClipper<Rect>{

  final double drawWidth;


  StarClipper(this.drawWidth);

  @override
  Rect getClip(Size size) {
    // TODO: implement getClip
    return Rect.fromLTWH(0, 0, drawWidth, size.height);
  }

  @override
  bool shouldReclip(covariant StarClipper oldClipper) {
    // TODO: implement shouldReclip
    return oldClipper.drawWidth != drawWidth;
  }

}
