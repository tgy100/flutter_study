import 'package:flutter/material.dart';
import 'dart:math';

class GridViewWidget extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // throw UnimplementedError();
    return GridView(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 300,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
        childAspectRatio: 0.5,
      ),
      children:generalWidget(100)!,
    );
  }

  List<Widget>? generalWidget(int count) {

    final widgets = <Widget>[];

    for(int i = 0; i < count; i++) {

      widgets.add(Container(
        width: 100.0 + Random().nextInt(100),
        height: 100.0 + Random().nextInt(100),
        color: Color.fromARGB(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255)),
      ));
    }

    return widgets;
  }
}

class GridBuildViewWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GridView.builder(
        itemCount: 20,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4,
          mainAxisSpacing: 10,
        ),
        itemBuilder: (context,index){
          return Container(
            height: 60,
            color: index % 2 == 0 ? Colors.blue:Colors.yellow,
          );
        });
  }

}