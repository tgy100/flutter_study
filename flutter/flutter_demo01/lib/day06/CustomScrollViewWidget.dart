import 'package:flutter/material.dart';

class CustomScrollViewWidget extends StatelessWidget {

  final String imageUrl = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1114%2F0512210S939%2F2105120S939-12-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1640231399&t=e13cb402c852a632c7121185427ed778";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 250,
            flexibleSpace: FlexibleSpaceBar(
              title: Text("sliver学习"),
              background: Image.network(
                imageUrl,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverGrid(delegate: SliverChildBuilderDelegate(
                  (context,index) {
                return Container(
                  width: 100,
                  color: index % 2 == 0? Colors.blue:Colors.red,
                );
              },
              childCount: 10
          ), gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 100,
            mainAxisSpacing: 10,
          )),
          SliverFixedExtentList(
              delegate: SliverChildBuilderDelegate(
                    (context,index) {
                  return ListTile(
                    leading: Icon(Icons.add),
                    title: Text("张三"),
                    subtitle: Text("1323423423423"),
                    trailing: Icon(Icons.pets),
                  );
                },
                childCount: 20,
              ),
              itemExtent: 50)
        ]
    );
  }
}