import 'package:flutter/material.dart';
import 'dart:math';

class MyApp extends StatefulWidget {

  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  final List<String> texts = ['a','b','c','d','e','f'];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Widget-Element-RenderObject"),
        ),
        body: HomeWidget(texts: texts,),
        floatingActionButton: FloatingActionButton(
          onPressed: (){
            setState(() {
              if (texts.length <= 0) {

                texts.add('a');
                texts.add('b');
                texts.add('c');
                texts.add('d');
                texts.add('e');
                texts.add('f');
              }else {
                texts.removeAt(0);
              }
            });

          },
          child: const Icon(Icons.pets),
        ),
      ),
    );
  }
}

class HomeWidget extends StatefulWidget {


  final List<String> texts;

  const HomeWidget({required this.texts,Key? key}) : super(key: key);

  @override
  State<HomeWidget> createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: widget.texts.map((e) {
        // return ListItemFullWidget(title: e);
        return ListItemFullWidget(title: e,key: ValueKey(e));
      }).toList(),
    );
  }

// List<Widget> buildItemWidget() {
//
//   return List.generate(widget.texts.length, (index) {
//
//     return Container(
//       key: ValueKey(index),
//       width: double.infinity,
//       height: 80,
//       color: Color.fromARGB(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255)),
//       child: Text(widget.texts[index]),
//     );
//   });
// }
}

class ListItemLessWidget extends StatelessWidget {

  final String title;

  const ListItemLessWidget({required this.title,Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: double.infinity,
      height: 80,
      color: Color.fromARGB(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255)),
      child: Text(title),
    );
  }
}

class ListItemFullWidget extends StatefulWidget{

  final String title;

  const ListItemFullWidget({required this.title,Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ListItemFullWidgetState();
  }
}

class _ListItemFullWidgetState extends State<ListItemFullWidget> {

  final Color randomColor = Color.fromARGB(255, Random().nextInt(255), Random().nextInt(255), Random().nextInt(255));

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Container(
      width: double.infinity,
      height: 80,
      color: randomColor,
      child: Text(widget.title),
    );
  }
}
