import 'dart:math';

import 'package:flutter/material.dart';


void main(List<String> argv) {

  runApp(MyApp());
}


class MyApp extends StatelessWidget {

  final GlobalKey<_HomeViewPageState> _homeViewPageGlobalKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("global key"),
        ),
        body: HomeViewPage(key: _homeViewPageGlobalKey,),
        floatingActionButton: FloatingActionButton(
          onPressed: () {

          },
        ),
      ),
    );
  }
}


class HomeViewPage extends StatefulWidget {

  const HomeViewPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeViewPageState();
  }
}

class _HomeViewPageState extends State<HomeViewPage> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: [],
    );
  }
}
