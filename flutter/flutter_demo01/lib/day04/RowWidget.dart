import 'package:flutter/material.dart';
class RowWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            // flex: 1,
            child: Container(
              height: 60,
              // color: Colors.yellow,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.yellow,
              ),
            ),
          ),
          Expanded(
            // flex: 1,
            child: Container(
              height: 90,
              color: Colors.purple,
            ),
          ),
          Expanded(
            // flex: 1,
              child: Container(
                height: 60,
                color: Colors.red,
              ))
        ],
      ),
    );
  }
}
