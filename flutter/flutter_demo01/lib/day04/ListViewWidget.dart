import 'package:flutter/material.dart';
import 'dart:convert';

class ListViewWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ListViewState();
  }
}

class _ListViewState extends State<ListViewWidget> {

  Map<String,dynamic>? listData;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }


  @override
  Widget build(BuildContext context) {

    if(listData == null) {

      DefaultAssetBundle.of(context).loadString("assets/jsons/persons.json").then((value) {

        setState(() {
          listData = json.decode(value);
        });
      });

    }

    return ListView(
      children: general(),
    );
  }

  List<Widget> general() {

    if(listData == null) {
      return [];
    }

    List children = listData?['children'];

    if (children == null || children.isEmpty) {

      return [];
    }

    final listItems = <Widget>[];

    for(Map<String,dynamic> inner in children) {

      if (inner == null) {
        continue;
      }

      final val = inner["name"];

      if (val != null) {

        listItems.add(Text(val));
      }
    }

    return listItems;
  }
}