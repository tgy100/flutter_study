import 'package:flutter/material.dart';

class ContainerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Container(
        width: 200,
        height: 200,
        child: Icon(
          Icons.pets,
          size: 36,
        ),
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.bottomRight,
                end: Alignment.topLeft,
                colors: [
                  Colors.red,
                  Colors.green
                ]
            ),
            boxShadow: [
              BoxShadow(
                  color: Colors.blueAccent,
                  offset: Offset(5,5)
              )
            ],
            // 设置圆角
            borderRadius: BorderRadius.circular(10),
            // 设置边框
            border: Border.all(
              width: 2,
              color: Colors.amberAccent,
              // style: BorderStyle.none
            ),
            color: Colors.deepOrangeAccent),
        // color: Colors.yellow,
        // color: Colors.green,
      ),
      widthFactor: 1,
      heightFactor: 1,
    );
  }
}
