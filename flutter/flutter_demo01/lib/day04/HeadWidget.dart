import 'package:flutter/material.dart';
import 'dart:convert';

class HeadWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    DefaultAssetBundle.of(context).loadString("assets/jsons/persons.json").then((value){

      print(value);
      final valString = json.decode(value);
      print(valString);
    });

    // TODO: implement build
    return Container(
      color: Colors.yellow,
      width: MediaQuery.of(context).size.width,
      height: 300,
      child: Stack(
        children: [
          Image.asset(
            "assets/images/beauty.jpg",
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.fitWidth,
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              height: 44,
              color: Colors.black.withOpacity(0.5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      "你好呀！！",
                      style: TextStyle(
                          color: Colors.white
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Icon(
                      Icons.pets,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

}
