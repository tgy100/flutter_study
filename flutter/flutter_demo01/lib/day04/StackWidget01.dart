import 'package:flutter/material.dart';

class StackWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: [
          Image.asset(
            "assets/images/beauty.jpg",
            fit: BoxFit.fitHeight,
            height: MediaQuery.of(context).size.height,
          ),
          Positioned(
              right: 10,
              top: 10,
              child: Container(
                width: 80,
                height: 80,
                // color: Colors.black.withOpacity(0.5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  color: Colors.black.withOpacity(0.6),
                ),
                child: TextButton(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "倒计时",
                        style: TextStyle(
                            color: Colors.white
                        ),
                      ),
                      Text("8s")
                    ],
                  ),
                  onPressed: (){

                  },
                ),
              )
          ),
        ],
      ),
    );
  }
}

class StackWidget01 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: [
        Container(
          width: 300,
          height: 300,
          color: Colors.yellow,
        ),
        Positioned(
          right: 10,
          bottom: 10,
          child:Icon(Icons.pets),
        ),
      ],
    );
  }

}
