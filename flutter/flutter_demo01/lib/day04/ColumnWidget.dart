import 'package:flutter/material.dart';

class ColumnWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
            child: Container(
              width: 100,
              height: 60,
              color: Colors.purple,
            )
        )
        ,
        Container(
          width: 200,
          height: 100,
          color: Colors.yellow,
        ),
        Container(
          width: 150,
          height: 150,
          color: Colors.blue,
        )
      ],
    );
  }
}
