import 'package:flutter/material.dart';

class ImageTitleWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: TextButton(
        style: ButtonStyle(
            backgroundColor:  MaterialStateProperty.all(Colors.green)
        ),
        onPressed: (){

        },
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(Icons.pets),
            Text("嘿嘿")
          ],
        ),
      ),
    );
  }
}

class RowWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          width: 100,
          height: 80,
          color: Colors.red,
        ),
        Container(
          width: 50,
          height: 100,
          color: Colors.green,
        ),
        Container(
          width: 80,
          height: 80,
          color: Colors.purple,
        ),
        Container(
          width: 70,
          height: 70,
          color: Colors.orange,
        ),
      ],
    );
  }

}
