import 'package:flutter/material.dart';

class CircleContainerWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Container(
        width: 200,
        height: 200,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            image: DecorationImage(
                image: ExactAssetImage(
                    "assets/images/beauty.jpg"
                )
            )
        ),
        // child: Image.asset("assets/images/beauty.jpg"),
      ),
    );
  }
}