import 'package:flutter/material.dart';

class NetImageWidget extends StatelessWidget {

  final String imageUrl = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1114%2F0512210S939%2F2105120S939-12-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1640231399&t=e13cb402c852a632c7121185427ed778";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Image.network(
        imageUrl,
        fit: BoxFit.fitWidth,
        // fit: BoxFit.contain,
        width: 200,
        height: 200,
        alignment: Alignment.topCenter,
        repeat: ImageRepeat.repeatY,
        color: Colors.red,
        colorBlendMode: BlendMode.colorDodge,
      ),
    );
  }
}