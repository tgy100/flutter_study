import 'package:flutter/material.dart';

class CircleWidget extends StatelessWidget {

  final String imageUrl = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1114%2F0512210S939%2F2105120S939-12-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1640231399&t=e13cb402c852a632c7121185427ed778";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Center(
      child: CircleAvatar(
        // backgroundImage:  ExactAssetImage(
        //   "assets/images/beauty.jpg"
        // ),
        backgroundImage: NetworkImage(
            imageUrl
        ),
        radius: 10,
        backgroundColor: Colors.red,
        child: Container(
          alignment: Alignment(0,0.5),
          width: 200,
          height: 200,
          child: Text(
              "哈哈哈"
          ),
        ),
      ),
    );
  }
}

class CircleWidget01 extends StatelessWidget {

  final String imageUrl = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1114%2F0512210S939%2F2105120S939-12-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1640231399&t=e13cb402c852a632c7121185427ed778";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: 100,
      height: 100,

      alignment: Alignment.center,
      child: ClipOval(
        clipper: _MyClipper(),
        child: Image.network(
          imageUrl,
          width: 100,
          height: 100,
        ),
        // child: Image.asset("assets/images/beauty.jpg"),
      ),
    );
  }

}


class _MyClipper extends CustomClipper<Rect>{
  @override
  Rect getClip(Size size) {
    print("getClip");
    return new Rect.fromLTWH(0, 0, 50, 50);
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) {
    return false;
  }
}
