import 'package:flutter/material.dart';

class TextFieldWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TextFieldState();
  }
}

class _TextFieldState extends State<TextFieldWidget> {

  final textEditingController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    textEditingController.text = "张三";
    textEditingController.addListener(() {
      print("addListener ${textEditingController.text}");
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextField(
      controller: textEditingController,
      decoration: InputDecoration(
        icon: Text("用户名"),
        hintText: "请输入用户名",
        // filled: true,
        // fillColor: Colors.grey,
        // border: InputBorder.none,
      ),
      onChanged: (val) {

        print("onChanged ${val}");
      },
      onSubmitted: (val) {

        print("onSubmitted ${val}");
      },
    );
  }
}
