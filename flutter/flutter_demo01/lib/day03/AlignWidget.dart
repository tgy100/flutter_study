import 'package:flutter/material.dart';

class AlignWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Center(
      child: Text("xx"),
    );


    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Align(
        child: Icon(
          Icons.pets,
          size: 36,
          color: Colors.yellow,
        ),
        alignment: Alignment.topCenter,
        widthFactor: 1.5,
        heightFactor: 1.5,
      ),
    );
  }

}
