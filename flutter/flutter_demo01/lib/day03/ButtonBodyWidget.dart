import 'package:flutter/material.dart';

class ButtonBodyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        FloatingActionButton(
          onPressed: (){

          },
          child: Icon(Icons.add),
          backgroundColor: Colors.black45,

        ),
        ElevatedButton(
          onPressed: (){
          },
          child: Text('点我'),
        ),
        Container(
          width: 100,
          height: 40,
          child: TextButton(
            onPressed: (){

            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.add),
                Text("我的"),
              ],
            ),
            onLongPress: () {
              print("长按");
            },
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.resolveWith((states) => Colors.purple),
                // padding: MaterialStateProperty.resolveWith((states) => EdgeInsets.only(left: 10)),
                shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18)
                    )
                ),
                side: MaterialStateProperty.all(
                  BorderSide(
                      width: 1,
                      color: Colors.red
                  ),
                )
            ),
          ),
        )
      ],
    );
  }



}