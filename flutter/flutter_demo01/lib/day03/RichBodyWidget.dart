import 'package:flutter/material.dart';

class RichBodyWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Row(
          children: [
            Text.rich(
              TextSpan(
                children: [
                  TextSpan(
                    text: "hello",
                    style: TextStyle(
                        fontSize: 10,
                        color: Colors.red
                    ),
                  ),
                  TextSpan(
                      text: "world",
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.purple
                      )
                  )
                ],
              ),
              textAlign: TextAlign.center,
              style: TextStyle(
                backgroundColor: Colors.black12,
              ),
            ),
          ],
        )
      ],
    );
  }

}