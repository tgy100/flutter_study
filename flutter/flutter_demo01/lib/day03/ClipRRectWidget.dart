import 'package:flutter/material.dart';

class ClipRRectWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(

      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Image.asset(
          "assets/images/beauty.jpg",
          width: 200,
          height: 200,
        ),
      ),
    );
  }
}
