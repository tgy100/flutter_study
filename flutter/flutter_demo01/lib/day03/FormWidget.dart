import 'package:flutter/material.dart';
class FormContentWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FormState();
  }
}


class _FormState extends State<FormContentWidget> {

  final registerFormKey = GlobalKey<FormState>();
  late String username = "";
  late String password = "";

  void login() {

    registerFormKey.currentState?.validate();
    registerFormKey.currentState?.save();
    print("username:${username},password:${password}");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // Form(child: child)

    return Form(
      key: registerFormKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                icon: Icon(Icons.people),
                hintText: "请输入用户名",
                border: InputBorder.none,
              ),
              onSaved: (val) {
                this.username = val??"";
              },
              validator: (val) {

                if (val == null ||  val.isEmpty) {

                  return "用户名不能为空";
                }
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                icon: Icon(Icons.lock),
                hintText: "请输入密码",
                border: InputBorder.none,
              ),
              onSaved: (val){
                this.password = val ?? "";
              },
              validator: (val) {

                if (val == null || val.isEmpty) {

                  return "密码不能为空";
                }
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: double.infinity,
              height: 44,
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue),
                ),
                onPressed: () {
                  this.login();
                },
                child: Text(
                  "登录",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}