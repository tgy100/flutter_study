import 'package:flutter/material.dart';


void main() {

  runApp(MyApp());
}


class MyApp extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }

}

class _MyAppState extends State<MyApp> {

  bool isChange = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 10),() {
      setState(() {
        isChange = true;
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("change"),
        ),
        body: isChange ? Row(
          children: [
            ChangefulWidget()
          ],
        ):Row(
          children: [
            Text(
              "没有change",
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.red
              ),
            ),
            ChangefulWidget()
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            print("click ...");
          },
          tooltip: 'increament',
          child: Icon(Icons.add),
        ),
      ),
    );
  }

}


class ChangefulWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ChangeState();
  }

}


class _ChangeState extends State<ChangefulWidget> {


  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

    print("didChangeDependencies call");
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("_ChangeState initState call");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(
      "change Text",
      style: TextStyle(
          fontSize: 30,
          color: Colors.yellow
      ),
    );
  }

}




