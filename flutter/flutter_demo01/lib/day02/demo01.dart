
import 'package:flutter/material.dart';


void main(List<String> args) {

  runApp(MyApp());
}


class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(
              "demo01"
          ),
        ),
        body: BodyfulWidget(),
      ),
    );
  }
}


class BodyfulWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BodyState();
  }
}

class _BodyState extends State<BodyfulWidget> {

  int count = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("initState call");
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

    print("didChangeDependencies call");
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    count++;
                  });

                },
                child: Text(
                    "+1"
                )
            ),
            ElevatedButton(
                onPressed: () {

                  setState(() {

                    if (count > 0) {
                      count--;
                    }

                  });
                },
                child: Text(
                    "-1"
                )
            ),
          ],
        ),
        Text("计数器:${count}")
      ],
    );
  }
}

