import 'package:flutter/material.dart';
import 'package:flutter_demo03/favorcate/core/model/tgy_meal_model.dart';
import '../services/json_parse.dart';
import '../model/tgy_meal_model.dart';

class TGYMealViewModel extends ChangeNotifier{

  List<TgyMealModel> _mealModels = <TgyMealModel>[];

  TGYMealViewModel() {

  }

  set mealModels(meals) {

    _mealModels = meals;
  }

  List<TgyMealModel> get mealModels {

    return _mealModels;
  }


}