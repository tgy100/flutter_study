
import 'package:flutter/material.dart';
import 'package:flutter_demo03/favorcate/core/model/tgy_meal_model.dart';

class TGYCollectionMealViewModel extends ChangeNotifier{

  List<TgyMealModel> _collectionModels = [];

  void addCollectionMeal(TgyMealModel mealModel) {

    _collectionModels.add(mealModel);
    notifyListeners();
  }

  List<TgyMealModel> getCollectionMeals() {

    return _collectionModels;
  }

  void removeCollectionMeal(TgyMealModel mealModel) {

    _collectionModels.remove(mealModel);
    notifyListeners();
  }

  bool isCollectionMeal(String id) {

    for(var meal in _collectionModels) {

      if (meal.id == id) {

        return true;
      }
    }

    return false;
  }

}