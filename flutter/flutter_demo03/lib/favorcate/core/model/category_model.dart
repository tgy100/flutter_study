
import 'dart:ui';

class TGYCategoryModel {
  String? id;
  String? title;
  String? color;
  Color? realColor;

  TGYCategoryModel({this.id, this.title, this.color});

  TGYCategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    color = json['color'];
    final colorInt = int.parse(color!,radix: 16);

    // realColor = Color.fromRGBO(colorInt & 0xff0000, colorInt & 0xff00, colorInt & 0xff,1.0);
    realColor = Color(colorInt | 0xff000000);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['color'] = this.color;
    return data;
  }
}