import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_demo03/favorcate/core/model/tgy_meal_model.dart';
import 'package:flutter_demo03/favorcate/ui/pages/home/tgy_home_todo.dart';

import '../../ui/pages/home/tgy_home_widget.dart';
import '../../ui/pages/favor/tgy_favor_widget.dart';
import '../../ui/pages/home/tgy_home_detail.dart';
class TGYRouter {

  static final Map<String, WidgetBuilder> routes = {
      TGYHomeWidget.route: (context) {
        return TGYHomeWidget();
      },
      TGYFavorWidget.route: (context) {
        return TGYFavorWidget();
      },
      TGYHomeDetailWidget.route:(context) {
        
        return TGYHomeDetailWidget();
      },
     // TGYHomeTodoWidget.route:(context) {
     //
     //    return
     // }
  };

/**
 * onGenerateRoute: (settings){

    if (settings.name == ProfileWidget.ROUTE) {

    return MaterialPageRoute(builder: (context){
    return ProfileWidget(settings.arguments as String);
    });
    }else if (settings.name == MallRouteWidget.route) {

    return MaterialPageRoute(builder: (context){

    return MallRouteWidget(settings.arguments as String);
    });
    }

    return null;
    },
    onUnknownRoute: (settings){

    return MaterialPageRoute(builder: (context){

    return UnknownWidget();
    });
    },
 */

static final RouteFactory onGenerateRoute = (settins) {

  if (settins.name == TGYHomeTodoWidget.route) {

    return MaterialPageRoute(builder: (context){

      return TGYHomeTodoWidget(settins.arguments as TgyMealModel);
    });
  }

  return null;
};

  static final RouteFactory onUnknownRoute = (settings){

    return null;
  };

}