import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_demo03/favorcate/core/model/tgy_meal_model.dart';
import 'package:flutter_demo03/favorcate/core/viewmodel/tgy_meal_viewmodel.dart';

import '../model/category_model.dart';
class TGYJsonParse {

  static Future<List<TGYCategoryModel>>? generalCategoryModels() async{

    final catagoryString = await rootBundle.loadString("asset/jsons/category.json");

    final jsonResult = json.decode(catagoryString)["category"].toList();

    List<TGYCategoryModel> categoryModels = [];

    for(var val in jsonResult) {

       categoryModels.add(TGYCategoryModel.fromJson(val));
    }

    return categoryModels;
  }

  static Future<List<TgyMealModel>> generalMeals() async {

    final meatsString = await rootBundle.loadString("asset/jsons/meal.json");

    final jsonResult = json.decode(meatsString)["meal"].toList();

    List<TgyMealModel> meals = [];

    for(var val in jsonResult) {

      meals.add(TgyMealModel.fromJson(val));
    }

    return meals;

  }

}