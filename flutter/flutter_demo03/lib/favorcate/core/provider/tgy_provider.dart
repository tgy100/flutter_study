import 'package:flutter_demo03/favorcate/core/viewmodel/tgy_collection_meal_viewmodel.dart';
import 'package:flutter_demo03/favorcate/core/viewmodel/tgy_meal_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class TGYProvider {
  static List<SingleChildWidget> providers = [
    ChangeNotifierProvider(create: (ctx) {
      return TGYMealViewModel();
    }),
    ChangeNotifierProvider(create: (ctx){

      return TGYCollectionMealViewModel();
    })
  ];
}
