import 'package:flutter/material.dart';
import 'package:flutter_demo03/favorcate/core/model/tgy_meal_model.dart';
import 'package:flutter_demo03/favorcate/core/viewmodel/tgy_collection_meal_viewmodel.dart';
import 'package:flutter_demo03/favorcate/core/viewmodel/tgy_meal_viewmodel.dart';
import 'package:flutter_demo03/favorcate/ui/pages/home/tgy_home_todo.dart';
import 'package:provider/provider.dart';


class TGYHomeDetailWidget extends StatefulWidget {

  static const String route = "/homeDetailWidget";

  const TGYHomeDetailWidget({Key? key}) : super(key: key);

  @override
  _TGYHomeDetailWidgetState createState() => _TGYHomeDetailWidgetState();
}

class _TGYHomeDetailWidgetState extends State<TGYHomeDetailWidget> {

  late String id;
  late final double screenWidth;

  @override
  Widget build(BuildContext context) {

    id = ModalRoute.of(context)!.settings.arguments as String;
    // screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text("详情"),
      ),
      body: generalContentSelect(),
    );
  }


  Widget generalContent() {

    return Consumer<TGYMealViewModel>(builder: (context,val,child){

      List<TgyMealModel> selectMealModels = [];
      for (var mealModel in val.mealModels) {
          if (mealModel.categories!.contains(id)) {

            selectMealModels.add(mealModel);
          }
      }

      return ListView.builder(itemBuilder: (context,index) {
        return Container(
          child: Text("${selectMealModels[index].title}"),
        );
      },itemCount: selectMealModels.length,itemExtent: 100,);
    },);
  }

  Widget generalContentSelect() {

    
    return Selector<TGYMealViewModel,List<TgyMealModel>>(builder: (BuildContext context, value, Widget? child){

      return ListView.builder(itemBuilder: (context,index){


        return Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10)
          ),
          child: generalContentItem(value[index]),
        );
        
      },itemCount: value.length,);
    }, selector: (context,val){

      List<TgyMealModel> selectMealModels = [];
      for (var mealModel in val.mealModels) {
        if (mealModel.categories!.contains(id)) {

          selectMealModels.add(mealModel);
        }
      }
      return selectMealModels;
    });
  }

  Widget generalContentItem(TgyMealModel mealModel) {


    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: (){

            Navigator.of(context).pushNamed(TGYHomeTodoWidget.route,arguments: mealModel);
          },
          child: Stack(
            children: [
              ClipRRect(
                child: Image.network(mealModel.imageUrl!,width: double.infinity,height: 200,fit: BoxFit.fitWidth,),
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
              ),
              Positioned(
                right: 5,
                bottom: 10,
                child: Container(
                  alignment: Alignment.center,
                  width: 200,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.black.withOpacity(0.8),
                  ),
                  child: Text(mealModel.title!,style: TextStyle(color: Colors.white,fontSize: 18),),
                ),
              )
            ],
          ),
        ),
        const SizedBox(height: 10,),
        generalBottom(mealModel)
      ],
    );
  }

  Widget generalBottom(TgyMealModel mealModel) {

    return Container(
      height: 44,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          generItem(Icon(Icons.pets), "未收藏"),
          generItem(Icon(Icons.pets), "未收藏"),
          Consumer<TGYCollectionMealViewModel>(builder: (context,val,child){

            Widget showWidget = Icon(Icons.pets);
            String showTitle= "未收藏";
            if(val.isCollectionMeal(mealModel.id!)) {
              showWidget = Icon(Icons.pets,color: Colors.red,);
             // return generItem(, "已收藏");
              showTitle = "已收藏";
            }
            return GestureDetector(child: generItem(showWidget, showTitle),onTap: (){

              final flag = val.isCollectionMeal(mealModel.id!);

              if(flag) {
                val.removeCollectionMeal(mealModel);
              }else{

                val.addCollectionMeal(mealModel);
              }
            },);
        }),
        ],
      ),
    );
  }

  Widget generItem(Widget widget,String title) {

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        widget,
        SizedBox(width: 3,),
        Text(title,style: TextStyle(fontSize: 14,color: Colors.black),)
      ],
    );
  }

}
