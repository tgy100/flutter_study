import 'package:flutter/material.dart';
import 'package:flutter_demo03/favorcate/ui/pages/home/tgy_home_content_widget.dart';
import './tgy_home_widget.dart';

class TGYHomeWidget extends StatelessWidget{

  static const String route = "/home";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(
        title: Text("首页"),
      ),
      body: TGYHomeContentWidget(),
    );
  }

}