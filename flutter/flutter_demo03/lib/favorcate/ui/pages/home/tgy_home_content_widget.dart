import 'package:flutter/material.dart';
import 'package:flutter_demo03/favorcate/core/model/category_model.dart';
import '../../../core/services/json_parse.dart';
import './tgy_home_detail.dart';
class TGYHomeContentWidget extends StatefulWidget {
  const TGYHomeContentWidget({Key? key}) : super(key: key);

  @override
  _TGYHomeContentWidgetState createState() => _TGYHomeContentWidgetState();
}

class _TGYHomeContentWidgetState extends State<TGYHomeContentWidget> {

  List<TGYCategoryModel> _categoryModels = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    TGYJsonParse.generalCategoryModels()!.then((value) {
        setState(() {
          _categoryModels.addAll(value);
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      padding: const EdgeInsets.all(8),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2,
      crossAxisSpacing: 10,
      childAspectRatio: 1.5,
      mainAxisSpacing: 10
    ), itemBuilder: (context,index){

      final categoryModel = _categoryModels[index];
      return GestureDetector(
        onTap: (){

          Navigator.of(context).pushNamed(TGYHomeDetailWidget.route,arguments: categoryModel.id);
        },
        child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            gradient: LinearGradient(colors: [
              categoryModel.realColor!.withOpacity(0.5),
              categoryModel.realColor!
            ])
          ),
          child: Text("${categoryModel.title}",style: Theme.of(context).textTheme.headline3!.copyWith(color: Colors.white),),
        ),
      );
    },itemCount: _categoryModels.length,);
  }
}
