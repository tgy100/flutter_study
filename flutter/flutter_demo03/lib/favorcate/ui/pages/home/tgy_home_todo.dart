import 'package:flutter/material.dart';
import 'package:flutter_demo03/favorcate/core/model/tgy_meal_model.dart';

class TGYHomeTodoWidget extends StatefulWidget {

  static const String route = "/homeTodoRoute";

  final TgyMealModel mealModel;

  double _screenWidth = 0;

  TGYHomeTodoWidget(this.mealModel,{Key? key}) : super(key: key);

  @override
  _TGYHomeTodoWidgetState createState() => _TGYHomeTodoWidgetState();
}

class _TGYHomeTodoWidgetState extends State<TGYHomeTodoWidget> {
  @override
  Widget build(BuildContext context) {


    widget._screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text("步骤"),
      ),
      body: generalBody(),
    );
  }

  Widget generalBody() {
    
    return SingleChildScrollView(
      child: Column(
        children: [
          Image.network(widget.mealModel.imageUrl!,height: 200,width: double.infinity,fit: BoxFit.fitWidth,),
          Container(
            alignment: Alignment.center,
            height: 40,
            child: Text("原料",style: TextStyle(fontSize: 20),),
          ),
          generalIngredients(),
          Container(
            alignment: Alignment.center,
            height: 40,
            child: Text("步骤",style: TextStyle(fontSize: 20),),
          ),
          generalStep()
        ],
      ),
    );
  }
  
  Widget generalIngredients(){

    return Container(
      width: widget._screenWidth - 20,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Colors.grey,width: 0.5),
      ),
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 8),
        shrinkWrap:true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context,index){

        final ingredient = widget.mealModel.ingredients![index];
        return Padding(
          padding: const EdgeInsets.only(bottom: 5),
          child: Container(
            padding: EdgeInsets.only(left: 10),
            alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                color: Colors.yellow,
                borderRadius: BorderRadius.circular(5)
              ),
              height: 44,
              child: Row(
                children: [
                  CircleAvatar(
                    foregroundColor: Colors.yellow,
                    backgroundColor: Colors.grey,
                    child: Text("${index}"),
                    radius: 14,
                  ),
                  SizedBox(width: 5,),
                  Text("${ingredient}",style: TextStyle(fontSize: 16),)
                ],
              )
          ),
        );
      },itemCount: widget.mealModel.ingredients!.length,),
    );
  }

  Widget generalStep(){

    return Container(
      width: widget._screenWidth - 20,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Colors.grey,width: 0.5),
      ),
      child: ListView.builder(
        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 8),
        shrinkWrap:true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context,index){
          final step = widget.mealModel.steps![index];

          return Padding(
            padding: const EdgeInsets.only(bottom: 5),
            child: Container(
                padding: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                    color: Colors.yellow,
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      CircleAvatar(
                        foregroundColor: Colors.yellow,
                        backgroundColor: Colors.grey,
                        child: Text("${index}"),
                        radius: 14,
                      ),
                      const SizedBox(width: 5,),
                      Expanded(child: Text("${step}",style: TextStyle(fontSize: 16)))
                    ],
                  ),
                )
            ),
          );
        },itemCount: widget.mealModel.steps!.length,),
    );
  }
  
}
