import 'package:flutter/material.dart';
import '../home/tgy_home_widget.dart';
import '../favor/tgy_favor_widget.dart';
import '../../shared/app_theme.dart';

class TGYMainWidget extends StatefulWidget {
  const TGYMainWidget({Key? key}) : super(key: key);

  @override
  State<TGYMainWidget> createState() => _TGYMainWidgetState();
}

class _TGYMainWidgetState extends State<TGYMainWidget> {
  int _selectIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build


    return Scaffold(
      body: IndexedStack(
        children: [
          TGYHomeWidget(),
          TGYFavorWidget(),
        ],
        index: _selectIndex,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                color: Colors.grey,
              ),
              activeIcon: Icon(
                Icons.home,
                color: TGYAppTheme.globalNormalColor,
              ),
              label: "首页"),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.star,
                color: Colors.grey,
              ),
              activeIcon: Icon(
                Icons.star,
                color: TGYAppTheme.globalNormalColor,
              ),
              label: "收藏"),
        ],
        currentIndex: _selectIndex,
        selectedFontSize: TGYAppTheme.bodyFontSize,
        unselectedFontSize: TGYAppTheme.bodyFontSize,
        onTap: (index) {
          setState(() {
            _selectIndex = index;
          });
        },
      ),
    );
  }
}
