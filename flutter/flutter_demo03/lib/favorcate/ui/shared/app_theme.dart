

import 'package:flutter/material.dart';

class TGYAppTheme {

  // 全局颜色
  static const globalNormalColor = Colors.pink;

  static const globalDarkColor = Colors.grey;

  static const double bodyFontSize = 14;
  static const double smallFontSize = 16;
  static const double normalFontSize = 20;
  static const double largeFontSize = 24;
  static const double xlargeFontSize = 30;

  static const TextTheme _textTheme = TextTheme(
    bodyText1: TextStyle(fontSize: bodyFontSize),
    headline1: TextStyle(fontSize: xlargeFontSize),
    headline2: TextStyle(fontSize: largeFontSize),
    headline3: TextStyle(fontSize: normalFontSize),
    headline4: TextStyle(fontSize: smallFontSize),
  );

  // normal
  static final ThemeData normalThemeData = ThemeData(
    primarySwatch: globalNormalColor,
    textTheme: _textTheme
  );

  // dark
  static final ThemeData darkThemeData = ThemeData(
    primarySwatch: globalDarkColor,

  );

}