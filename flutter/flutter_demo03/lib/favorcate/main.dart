import 'package:flutter/material.dart';
import 'package:flutter_demo03/favorcate/core/provider/tgy_provider.dart';
import 'package:provider/provider.dart';
import './ui/shared/app_theme.dart';
import './core/router/router.dart';
import './ui/pages/main/tgy_main_widget.dart';
import './core/viewmodel/tgy_meal_viewmodel.dart';
import 'core/services/json_parse.dart';

void main() {
  runApp(MultiProvider(
    providers: TGYProvider.providers,
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    // Provider.of<TGYMealViewModel>(context).mealModels =
    initData(context);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: TGYAppTheme.normalThemeData,
      routes: TGYRouter.routes,
      onGenerateRoute: TGYRouter.onGenerateRoute,
      onUnknownRoute: TGYRouter.onUnknownRoute,
      home: const TGYMainWidget(),
    );
  }

  void initData(BuildContext context) async {

      Provider.of<TGYMealViewModel>(context).mealModels =  await TGYJsonParse.generalMeals();
  }

}
