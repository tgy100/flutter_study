import 'package:flutter/material.dart';
import './hero_home_widget.dart';

void main() {

  runApp(HeroMainWidget());
}

class HeroMainWidget extends StatelessWidget {
  const HeroMainWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HeroHomeWidget(),
    );
  }
}
