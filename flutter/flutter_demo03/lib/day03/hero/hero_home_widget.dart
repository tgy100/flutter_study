import 'package:flutter/material.dart';
import './hero_mall_widget.dart';
class HeroHomeWidget extends StatefulWidget {
  const HeroHomeWidget({Key? key}) : super(key: key);

  @override
  _HeroHomeWidgetState createState() => _HeroHomeWidgetState();
}

class _HeroHomeWidgetState extends State<HeroHomeWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("hero"),
      ),
      body: generalWidget(),
    );
  }
  
  
  Widget generalWidget(){
    
    return GridView.builder(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2,
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      childAspectRatio: 1.5
    ), itemBuilder: (context,index){

      final String imageURL = "https://picsum.photos/id/$index/400/200";

      return GestureDetector(
        onTap: (){

          Navigator.of(context).push(PageRouteBuilder(pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation){

            return FadeTransition(opacity: animation,child: HeroMallWiget(imageURL),);
          }));
        },
        child:Hero(tag: imageURL , child: Image.network(imageURL,fit: BoxFit.fill,)) ,
      );
    },itemCount: 30,);
  }
}




