import 'package:flutter/material.dart';

class HeroMallWiget extends StatefulWidget {
  
  final String imageUrl;
  
  const HeroMallWiget(this.imageUrl,{Key? key}) : super(key: key);

  @override
  _HeroMallWigetState createState() => _HeroMallWigetState();
}

class _HeroMallWigetState extends State<HeroMallWiget> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: (){

        Navigator.of(context).pop();
      },
      onDoubleTap: (){

      },
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Center(
          child: Image.network(widget.imageUrl,fit: BoxFit.fitWidth,),
        ),
      ),
    );
  }
}
