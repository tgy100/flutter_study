import 'package:flutter/material.dart';

class ProfileWidget extends StatefulWidget {

  static const String ROUTE = "/profile";

  String message;

  ProfileWidget(this.message,{Key? key}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        Navigator.of(context).pop("left button");
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("个人中心"),
          actions: [
            ElevatedButton(onPressed: (){

              Navigator.of(context).pop("asdasd");
            }, child: Text("返回"))
          ],
        ),
        body: ProfileContentWidget(widget.message),
      ),
    );
  }
}

class ProfileContentWidget extends StatefulWidget {

  String message;

  ProfileContentWidget(this.message,{Key? key}) : super(key: key);

  @override
  State<ProfileContentWidget> createState() => _ProfileContentWidgetState();
}

class _ProfileContentWidgetState extends State<ProfileContentWidget> {
  @override
  Widget build(BuildContext context) {

    String showMessage = widget.message;
    var modalRoute = ModalRoute.of(context);
    print("_profile ${modalRoute}");

    if (modalRoute != null) {

      showMessage = modalRoute.settings.arguments as String;
    }
    print(showMessage);
    return Center(
      child: Row(
        children: [
          Text(showMessage),
        ],
      ),
    );
  }
}


