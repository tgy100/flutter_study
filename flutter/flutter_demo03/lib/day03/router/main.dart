import 'package:flutter/material.dart';
import 'package:flutter_demo03/day03/router/advert_widget.dart';
import './home_widget.dart';
import './profile_widget.dart';
import '../common/global_event.dart';
import './mall_route_widget.dart';
import './unknown_widget.dart';

void main(){

  runApp(MainWidget());
}


class MainWidget extends StatefulWidget {

  @override
  State<MainWidget> createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> {

  String initialRoute = AdvertWidget.route;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("initState");
    GlobalEvent.listenChangeToHome((obj) {

      final event = obj as ChangeToHomeEvent;
      setState(() {
        initialRoute = event.route;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Widget showWidget = AdvertWidget();
    if (initialRoute == HomeRouteWidget.route) {
      showWidget = HomeRouteWidget();
    }

    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.amber[100]
      ),
      routes: {
        ProfileWidget.ROUTE:(content) {
          return ProfileWidget("main go");
        },
        AdvertWidget.route:(context){
          return AdvertWidget();
        },
        HomeRouteWidget.route: (context){

          return HomeRouteWidget();
        }
      },
      onGenerateRoute: (settings){

        if (settings.name == ProfileWidget.ROUTE) {

          return MaterialPageRoute(builder: (context){
            return ProfileWidget(settings.arguments as String);
          });
        }else if (settings.name == MallRouteWidget.route) {

          return MaterialPageRoute(builder: (context){

            return MallRouteWidget(settings.arguments as String);
          });
        }

        return null;
      },
      onUnknownRoute: (settings){

        return MaterialPageRoute(builder: (context){

          return UnknownWidget();
        });
      },
      // initialRoute: initialRoute,
      home:  showWidget,
    );
  }
}


