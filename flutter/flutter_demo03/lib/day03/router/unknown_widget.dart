import 'package:flutter/material.dart';

class UnknownWidget extends StatefulWidget {
  const UnknownWidget({Key? key}) : super(key: key);

  @override
  _UnknownWidgetState createState() => _UnknownWidgetState();
}

class _UnknownWidgetState extends State<UnknownWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text("错误的路由"),
    );
  }
}
