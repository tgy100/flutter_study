import 'package:flutter/material.dart';

class MallRouteWidget extends StatefulWidget {

  static const String route = "/mall";

  String message;
  
  MallRouteWidget(this.message,{Key? key}) : super(key: key);

  @override
  _MallRouteWidgetState createState() => _MallRouteWidgetState();
}

class _MallRouteWidgetState extends State<MallRouteWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.message),
      ),
    );
  }
}
