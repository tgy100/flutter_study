import 'package:flutter/material.dart';
import 'package:flutter_demo03/day03/diffevent/main.dart';
import '../common/global_event.dart';
import './home_widget.dart' as route_home_widget;

class AdvertWidget extends StatefulWidget {

  static const String route = "/advert";

  const AdvertWidget({Key? key}) : super(key: key);

  @override
  _AdvertWidgetState createState() => _AdvertWidgetState();
}

class _AdvertWidgetState extends State<AdvertWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: Center(
        child: ElevatedButton(onPressed: (){


          final changeToHomeEvent = ChangeToHomeEvent(route_home_widget.HomeRouteWidget.route);
          GlobalEvent.fireChangeToHome(changeToHomeEvent);


        },child: const Text("进入主页"),),
      ),
    );
  }
}
