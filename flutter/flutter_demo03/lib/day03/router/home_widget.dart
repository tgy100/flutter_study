import 'package:flutter/material.dart';
import 'package:flutter_demo03/day03/router/mall_route_widget.dart';
import 'about_widget.dart';
import './profile_widget.dart';

class HomeRouteWidget extends StatelessWidget {

  static const String route = "/home";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("home"),
      ),
      body: const HomeStatefulWidget(),
    );
  }
}

class HomeStatefulWidget extends StatefulWidget {
  const HomeStatefulWidget({Key? key}) : super(key: key);

  @override
  _HomeStatefulWidgetState createState() => _HomeStatefulWidgetState();
}

class _HomeStatefulWidgetState extends State<HomeStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          ElevatedButton(
            onPressed: () {
              var pushResult = Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) {
                    return AboutWidget();
                  }));

              pushResult.then((value) =>
              {

                print(value)
              });
            },
            child: Text("点我"),
          ),
          ElevatedButton(onPressed: () {
            // Navigator.of(context);
            final futureRes = Navigator.push(
                context, MaterialPageRoute(builder: (context) {
              return ProfileWidget("首页过去的");
            }));

            futureRes.then((value) =>
            {

              print(value)
            });
          }, child: Text("个人中心")),
          ElevatedButton(onPressed: () {
            Navigator.of(context).pushNamed(
                ProfileWidget.ROUTE, arguments: "命名路由过去的");
          }, child: const Text("命名路由")),
          ElevatedButton(onPressed: () {
            Navigator.of(context).pushNamed(
                MallRouteWidget.route, arguments: "首页去商场");
          }, child: Text("mall")),
          ElevatedButton(onPressed: () {
            Navigator.of(context).pushNamed("/abc");
          }, child: Text("未知的路由"))
        ],
      ),
    );
  }
}
