import 'package:flutter/material.dart';


class ThemeHomeWidget extends StatefulWidget {
  const ThemeHomeWidget({Key? key}) : super(key: key);

  @override
  _ThemeHomeWidgetState createState() => _ThemeHomeWidgetState();
}

class _ThemeHomeWidgetState extends State<ThemeHomeWidget> {
  @override
  Widget build(BuildContext context) {
    final bodyText = Theme.of(context).textTheme.bodyText1;
    return Theme(
      data: Theme.of(context).copyWith(
        // primaryColor: Colors.purple
      ),
      child: Scaffold(
        appBar: AppBar(
          title: Text("首页",style:bodyText),
        ),
        body: Text("ddd",style: bodyText,),
      ),
    );
  }
}

