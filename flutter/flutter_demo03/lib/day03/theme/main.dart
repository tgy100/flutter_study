import 'dart:ui';

import 'package:flutter/material.dart';
import './theme_profile_widget.dart';
import './theme_home_widget.dart';

void main() {
  runApp(const MainWidget());
}

class MainWidget extends StatefulWidget {
  const MainWidget({Key? key}) : super(key: key);

  @override
  State<MainWidget> createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> {
  int _selectIndex = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          // brightness: Brightness.light,
          primarySwatch: Colors.green,
          primaryColor: Colors.purple,
          elevatedButtonTheme: ElevatedButtonThemeData(
              style: ButtonStyle(
                  shadowColor: MaterialStateProperty.all(Colors.transparent),
                  splashFactory: NoSplash.splashFactory,
                  foregroundColor: MaterialStateProperty.resolveWith((states) {
                    return states.contains(MaterialState.pressed)
                        ? Colors.yellow
                        : Colors.green;
                  }),
                  backgroundColor: MaterialStateProperty.resolveWith(
                    (states) {
                      return states.contains(MaterialState.pressed)
                          ? Colors.grey
                          : Colors.brown;
                    },
                  ),
                padding: MaterialStateProperty.all(EdgeInsets.symmetric(horizontal: 10,vertical: 10))
              )
          ),
          textTheme: TextTheme(
            headline1: TextStyle(
              fontSize: 12,
              color: Colors.green,
              backgroundColor: Colors.yellow
            ),
            headline2: TextStyle(
              fontSize: 14,
              color: Colors.purpleAccent
            ),
            bodyText1: TextStyle(
              fontSize: 16,
              backgroundColor: Colors.deepOrange,
              color: Colors.white
            )
          )
      ),
      darkTheme: ThemeData(
        primarySwatch: Colors.grey,
        textTheme: TextTheme(
          bodyText1: TextStyle(
            backgroundColor: Colors.blue,
            color: Colors.black,
          )
        )
      ),
      home: Scaffold(
        body: IndexedStack(
          children: [
            ThemeHomeWidget(),
            ThemeProfileWidget(),
          ],
          index: _selectIndex,
        ),
        bottomNavigationBar: BottomNavigationBar(
          selectedFontSize: 14,
          unselectedFontSize: 14,
          currentIndex: _selectIndex,
          items: [
            BottomNavigationBarItem(
                label: "首页",
                icon: Image.asset(
                  "asset/images/tabbar/home.png",
                  width: 30,
                ),
                activeIcon: Image.asset(
                  "asset/images/tabbar/home_active.png",
                  width: 30,
                )),
            BottomNavigationBarItem(
              label: "我的",
              icon: Image.asset(
                "asset/images/tabbar/profile.png",
                width: 30,
              ),
              activeIcon: Image.asset(
                "asset/images/tabbar/profile_active.png",
                width: 30,
              ),
            )
          ],
          onTap: (index) {
            print(index);
            setState(() {
              _selectIndex = index;
            });
          },
        ),
      ),
    );
  }
}
