import 'package:flutter/material.dart';

class ThemeProfileWidget extends StatefulWidget {
  const ThemeProfileWidget({Key? key}) : super(key: key);

  @override
  _ThemeProfileWidgetState createState() => _ThemeProfileWidgetState();
}

class _ThemeProfileWidgetState extends State<ThemeProfileWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("我的"),
      ),
      body: HomeContentWidget(),
    );
  }
}


class HomeContentWidget extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    
    
    return Column(
      children: [
        ElevatedButton(onPressed: (){
          print("点我");
        }, child: Text("点我")),
        OutlinedButton(onPressed: (){}, child: Text("out line")),
        ElevatedButton(
          child: Text("审"),
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Color(0xffffffff)),                //背景颜色
            foregroundColor: MaterialStateProperty.all(Color(0xff5E6573)),                //字体颜色
            overlayColor: MaterialStateProperty.all(Color(0xffffffff)),                   // 高亮色
            shadowColor: MaterialStateProperty.all( Color(0xffffffff)),                  //阴影颜色
            elevation: MaterialStateProperty.all(0),                                     //阴影值
            textStyle: MaterialStateProperty.all(TextStyle(fontSize: 12)),                //字体
            side: MaterialStateProperty.all(BorderSide(width: 1,color: Color(0xffCAD0DB))),//边框
            shape: MaterialStateProperty.all(
                const CircleBorder(
                    side: BorderSide(
                      //设置 界面效果
                      color: Colors.green,
                      style: BorderStyle.none,
                    )
                )
            ),//圆角弧度
          ),
          onPressed: () {},
        ),
        ElevatedButton(
            child: Text("审核完成"),
            onPressed: (){

            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Color(0xffffffff)),                //背景颜色
              foregroundColor: MaterialStateProperty.all(Color(0xff5E6573)),                //字体颜色
              overlayColor: MaterialStateProperty.all(Color(0xffffffff)),                   // 高亮色
              shadowColor: MaterialStateProperty.all( Color(0xffffffff)),                  //阴影颜色
              elevation: MaterialStateProperty.all(0),                                     //阴影值
              textStyle: MaterialStateProperty.all(TextStyle(fontSize: 12)),                //字体
              side: MaterialStateProperty.all(BorderSide(width: 1,color: Color(0xffCAD0DB))),//边框
              shape: MaterialStateProperty.all(
                  const StadiumBorder(
                      side: BorderSide(
                        //设置 界面效果
                        style: BorderStyle.solid,
                        color: Color(0xffFF7F24),
                      )
                  )
              ),//圆角弧度
            )
        ),
        Text("asd",style: Theme.of(context).textTheme.headline1,)
      ],
    );
  }

}
