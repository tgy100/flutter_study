
import 'package:event_bus/event_bus.dart';

typedef StringCallBack = void Function(String content);
typedef DynamicCallBack = void Function(dynamic obj);

class GlobalEvent{

  static final EventBus eventBus = EventBus();

  static void fireString(String content) {

    eventBus.fire(content);
  }

  static void listenString(StringCallBack callBack) {
    eventBus.on<String>().listen((event) {
      callBack(event);
    });
  }


  static void fireChangeToHome(ChangeToHomeEvent changeToHomeEvent) {

    eventBus.fire(changeToHomeEvent);
  }

  static void listenChangeToHome(DynamicCallBack dynamicCallBack) {

    eventBus.on<ChangeToHomeEvent>().listen((event) {
      dynamicCallBack(event);
    });
  }

}


class ChangeToHomeEvent {

  String route;
  ChangeToHomeEvent(this.route);
}