import 'package:flutter/material.dart';

void main() {
  runApp(const MainWidget());
}

class MainWidget extends StatelessWidget {
  const MainWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: const Text("event"),
          ),
          body: NestWidget01()),
    );
  }
}

class HomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Listener(
        onPointerDown: (details) {
          print("down ${details.localPosition}");
        },
        onPointerUp: (details) {
          print("up ${details.localPosition}");
        },
        onPointerMove: (details) {
          print("move ${details.localPosition}");
        },
        child: Container(
          width: 200,
          height: 200,
          color: Colors.yellow,
        ),
      ),
    );
  }
}

class HomeGestureWidget extends StatelessWidget {
  const HomeGestureWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: GestureDetector(
        onLongPress: () {
          print("long press");
        },
        onDoubleTap: () {
          print("dounle tap");
        },
        onTapUp: (details) {
          print("onTapUp ${details.localPosition}");
        },
        child: Container(
          width: 200,
          height: 200,
          decoration: BoxDecoration(
              color: Colors.green, borderRadius: BorderRadius.circular(10)),
        ),
      ),
    );
  }
}

class NestWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: GestureDetector(
        onTap: () {
          print("parent on tap");
        },
        child: Container(
          width: 200,
          height: 200,
          color: Colors.green,
          alignment: Alignment.center,
          child: GestureDetector(
            onTap: () {
              print("child on tap");
            },
            child: Container(
              width: 100,
              height: 100,
              color: Colors.purple,
            ),
          ),
        ),
      ),
    );
  }
}

class NestWidget01 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      alignment: Alignment.center,
      children: [
        GestureDetector(
          onTap: (){

            print("000000");
          },
          child: Container(
            width: 200,
            height: 200,
            color: Colors.purple,
          ),
        ),
        GestureDetector(
          onTap: (){

            print("11111");
          },
          child: Container(
            width: 100,
            height: 100,
            color: Colors.yellow,
          ),
        )
      ],
    );
  }
}
