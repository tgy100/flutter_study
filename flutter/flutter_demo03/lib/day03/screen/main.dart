import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import './screen_home_widget.dart';
import './tgy_fit_size.dart';

void main() {
  runApp(MainWidget());
}

class MainWidget extends StatefulWidget {
  const MainWidget({Key? key}) : super(key: key);

  @override
  _MainWidgetState createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> {
  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context);
    //
    // print("${size}");

    // final size = window.physicalSize;
    //
    // print("size ${size.width/window.devicePixelRatio} ${size.height/window.devicePixelRatio}");

    TGYFitSize.initialize(context);
    // return ScreenUtilInit(
    //   designSize: Size(360, 690),
    //   minTextAdapt: true,
    //   splitScreenMode: true,
    //   builder: (){
    //     return  MaterialApp(
    //       builder: (context,widget){
    //         //add this line
    //         ScreenUtil.setContext(context);
    //         return MediaQuery(
    //           //Setting font does not change with system font size
    //           data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
    //           child: widget!,
    //         );
    //       },
    //       home: Scaffold(
    //         body: ScreenHomeWidget(),
    //       ),
    //     );
    //   },
    // );


    return MaterialApp(
      home: Scaffold(
        body: ScreenHomeWidget(),
      ),
    );
  }
}
