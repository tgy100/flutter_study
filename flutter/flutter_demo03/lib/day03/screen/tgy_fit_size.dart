

import 'dart:ui';

import 'package:flutter/cupertino.dart';

class TGYFitSize {

  static double screenWidth = 0;
  static double screenHeight = 0;
  static double rpx = 0;
  static double px = 0;


  static void initialize(BuildContext context, {double standardWidth = 375}) {

    screenWidth = window.physicalSize.width / window.devicePixelRatio;
    screenHeight = window.physicalSize.height / window.devicePixelRatio;
    print(window.physicalSize);
    rpx = screenWidth / standardWidth;
  }

  static double setPx(double size){

    final lastSize = rpx * size;
    print(lastSize);
    return lastSize;
  }


}