import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import './tgy_fit_size.dart';

class ScreenHomeWidget extends StatefulWidget {
  const ScreenHomeWidget({Key? key}) : super(key: key);

  @override
  _ScreenHomeWidgetState createState() => _ScreenHomeWidgetState();
}

class _ScreenHomeWidgetState extends State<ScreenHomeWidget> {
  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    print("width height ${size.width} ${size.height}");

    ScreenUtil.init(
        BoxConstraints(
            maxWidth: TGYFitSize.screenWidth,
            maxHeight: TGYFitSize.screenHeight),
        designSize: Size(360, 690),
        context: context,
        minTextAdapt: true,
        orientation: Orientation.portrait);

    return Scaffold(
      appBar: AppBar(
        title: Text("首页"),
      ),
      body: Center(
        child: Container(
          // width: TGYFitSize.setPx(200),
          // height: TGYFitSize.setPx(200),
          width: ScreenUtil().setWidth(200),
          height: ScreenUtil().setWidth(200),
          color: Colors.red,
        ),
      ),
    );
  }
}
