import 'package:flutter/material.dart';
import 'package:flutter_demo03/day03/animation/animation_profile_widget.dart';


class AnimationHomeWidget extends StatefulWidget{
  const AnimationHomeWidget({Key? key}) : super(key: key);


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AnimationHomeState();
  }
}


class AnimationHomeState extends State<AnimationHomeWidget> with SingleTickerProviderStateMixin{

  late AnimationController _animationController;

  late Animation<double> _animation;

  get animationController {

    return _animationController;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _animationController = AnimationController(vsync: this,duration: Duration(seconds: 1));
    _animation = CurvedAnimation(parent: _animationController, curve: Curves.easeIn,reverseCurve: Curves.easeOut);
    // _animation.addListener(() {
    //
    //   setState(() {
    //
    //   });
    // });

    _animation.addStatusListener((status) {

      if (status == AnimationStatus.completed) {

        _animationController.reverse();
      }else if (status == AnimationStatus.dismissed) {

        _animationController.forward();
      }

    });

    _animation = Tween(begin: 30.0,end: 100.0).animate(_animationController);

  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // return Center(
    //   child: Icon(Icons.star,color: Colors.red,size: _animation.value,),
    // );
    print("AnimationState");
    // return Center(
    //   child: IconAnimation(_animation),
    // );
    return Center(
      child: Column(
        children: [
          AnimatedBuilder(
            animation: _animation,
            builder: (BuildContext context, Widget? child) {
              return Icon(Icons.star,color: Colors.blue,size: _animation.value);
            },
          ),
          ElevatedButton(onPressed: (){

            Navigator.of(context).push(MaterialPageRoute(builder: (context){

              return AnimationProfileWidget();
            }));
          },  child: Text("我的"))
        ],
      ),
    );
  }
}


class IconAnimation extends AnimatedWidget{
  
  IconAnimation(Animation animation):super(listenable: animation);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final animation = listenable as Animation;
    return Icon(Icons.star,color: Colors.red,size: animation.value,);
  }
  
}

