import 'package:flutter/material.dart';
import './animation_home_widget.dart';

void main() {
  runApp(MainWidget());
}

class MainWidget extends StatefulWidget {
  @override
  State<MainWidget> createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> {

  GlobalKey<AnimationHomeState> _globalKey = GlobalKey<AnimationHomeState>();


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("animation"),
        ),
        body: AnimationHomeWidget(key: _globalKey,),
        floatingActionButton: FloatingActionButton(
          onPressed: () {

            // final controller = _globalKey.currentState!.getAnimationController();

            final controller = _globalKey.currentState!.animationController as AnimationController;
            if (controller.isAnimating) {

              controller.stop();
            }else {

              controller.forward();
            }
          },
          child: const Icon(
            Icons.pets,
            size: 32,
          ),
        ),
      ),
    );
  }
}
