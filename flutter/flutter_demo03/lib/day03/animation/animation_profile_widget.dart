import 'package:flutter/material.dart';

class AnimationProfileWidget extends StatefulWidget {
  const AnimationProfileWidget({Key? key}) : super(key: key);

  @override
  _AnimationProfileWidgetState createState() => _AnimationProfileWidgetState();
}

class _AnimationProfileWidgetState extends State<AnimationProfileWidget> with SingleTickerProviderStateMixin {

  late AnimationController _animationController;
  late Animation<double> _animation;
  late Animation<Color?> _colorAnimation;
  late Animation<double> _sizeAnimation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _animationController = AnimationController(vsync: this,duration: Duration(seconds: 1));

    _animation = CurvedAnimation(parent: _animationController, curve: Curves.easeIn);

    _animation.addStatusListener((status) {

    });

    _colorAnimation = ColorTween(begin: Colors.blue,end: Colors.purple).animate(_animationController);

    _sizeAnimation  = Tween(begin: 80.0,end: 200.0).animate(_animationController);

  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("我的"),
      ),
      body: Stack(
        children: [
          Center(
            child:AnimatedBuilder(animation: _animation,builder: (context,child){

              return Container(
                width: _sizeAnimation.value,
                height: _sizeAnimation.value,
                color: _colorAnimation.value,
                child: Text("组合动画"),
              );
            },),
          ),
          Positioned(
            left: 10,
            bottom: 20,
            child: ElevatedButton(onPressed: (){

              _animationController.forward();

            }, child: Text("开始")),
          )
        ],
      ),
    );
  }
}
