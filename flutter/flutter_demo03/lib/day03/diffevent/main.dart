import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo03/day03/common/global_event.dart';


void main() {

  runApp(MainWidget());
}




class MainWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("跨组件事件"),
        ),
        body: HomeWidget(),
      ),
    );
  }
}

class HomeWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        SubWidget(),
        SizedBox(height: 10,),
        SubWidget01()
      ],
    );
  }

}


class SubWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: double.infinity,
      height: 80,
      color: Colors.green,
      child: ElevatedButton(onPressed: () {

        GlobalEvent.fireString("哈哈");
      }, child: Text("点击我"),),
    );
  }

}

class SubWidget01 extends StatefulWidget {
  const SubWidget01({Key? key}) : super(key: key);

  @override
  _SubWidget01State createState() => _SubWidget01State();
}

class _SubWidget01State extends State<SubWidget01> {

  String? message;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    GlobalEvent.listenString((content) {

      setState(() {

        message = content;
      });
    });
  }



  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
      height: 100,
      width: double.infinity,
      child: Text(message ?? "",style: TextStyle(color: Colors.white),),
    );
  }
}
