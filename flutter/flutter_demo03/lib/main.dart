import 'package:flutter/material.dart';

void main() {



  runApp(MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("demo01"),
        ),
        body: HomeStatelessWidget(),
      ),
    );
  }
}

class HomeStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Container(
          width: 10,
          height: 10,
          child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.purple),
                  // shape: MaterialStateProperty.all(ContinuousRectangleBorder(borderRadius: BorderRadius.circular(60)))
              ),
              onPressed: () {},
              child: Text("")),
        )

      ],
    );
  }
}
