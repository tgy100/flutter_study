
import '../model/home/home_item_model.dart';

import './http_request.dart';

class HomeRequest{

  // List<HomeItemModel>
  static Future<List<HomeItemModel>> requestHomeItem(Map<String,dynamic>? params) async{

      try {

        final data = await HttpRequest.request("/movie/top250",params: params);
        return HomeItemModel.generalHomeItemModelsFromList(data);
      }catch(e) {
        throw e;
      }
  }

}