import 'package:dio/dio.dart';
import './config.dart';

class HttpRequest {
  static final Dio dio = Dio(BaseOptions(
      baseUrl: HttpConfig.baseUrl, connectTimeout: HttpConfig.timeOut));

  static Future<dynamic> request(url,
      {String method = "get",
      Map<String, dynamic>? params,
      Interceptor? interceptor}) async {
    final options = Options(method: method);

    Interceptor inInterceptor = InterceptorsWrapper(
      onResponse: (Response e, ResponseInterceptorHandler handler) {
        return handler.next(e);
      },
      onRequest: (
        RequestOptions options,
        RequestInterceptorHandler handler,
      ) {
        handler.next(options);
      },
    );

    List<Interceptor> interceptors = [inInterceptor];

    if (interceptor != null) {
      interceptors.add(inInterceptor);
    }

    dio.interceptors.addAll(interceptors);

    try{

       Response response =  await dio.request(url,queryParameters: params,options: options);

       Map<String,dynamic> dataMap = response.data as Map<String,dynamic>;

       if (int.parse(dataMap["code"].toString()) == 0) {
         return dataMap["data"];
       }else{

         print(dataMap["message"].toString());
         throw Exception(dataMap["message"].toString());
       }

    }  catch(e) {

        throw e;
    }

  }
}
