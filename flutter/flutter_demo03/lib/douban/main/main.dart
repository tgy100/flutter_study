import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo03/douban/page/group/group_page.dart';
import 'package:flutter_demo03/douban/page/home/home_page.dart';
import 'package:flutter_demo03/douban/page/mall/mall_page.dart';
import 'package:flutter_demo03/douban/page/profile/profile_page.dart';
import 'package:flutter_demo03/douban/page/subject/subject_page.dart';

class MainStatefulWidget extends StatefulWidget {
  const MainStatefulWidget({Key? key}) : super(key: key);

  @override
  _MainStatefulWidgetState createState() => _MainStatefulWidgetState();
}

class _MainStatefulWidgetState extends State<MainStatefulWidget> {
  int _selectIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _selectIndex,
        children: createPages(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedFontSize: 14,
        unselectedFontSize: 14,
        currentIndex: _selectIndex,
        type: BottomNavigationBarType.fixed,
        // selectedLabelStyle: const TextStyle(color: Colors.green),
        // unselectedLabelStyle: const TextStyle(color: Colors.grey),
        unselectedItemColor: Colors.grey,
        selectedItemColor: Colors.green,
        onTap: (index) {
          setState(() {
            _selectIndex = index;
          });
        },
        items: createBottomNavigationBarItems(),
      ),
    );
  }

  List<Widget> createPages() {
    return <Widget>[
      HomePageStatelessWidget(),
      SubjectPageStatelessWidget(),
      GroupPageStatlessWidget(),
      MallPageStatelessWidget(),
      ProfilePageStatelessWidget()
    ];
  }

  List<BottomNavigationBarItem> createBottomNavigationBarItems() {
    return <BottomNavigationBarItem>[
      BottomNavigationBarItem(
          icon: Image.asset(
            "asset/images/tabbar/home.png",
            width: 30,
              gaplessPlayback: true
          ),
          activeIcon: Image.asset(
            "asset/images/tabbar/home_active.png",
            width: 30,
              gaplessPlayback: true
          ),
          label: "首页"),
      BottomNavigationBarItem(
          icon: Image.asset(
            "asset/images/tabbar/subject.png",
            width: 30,
              gaplessPlayback: true
          ),
          activeIcon: Image.asset(
            "asset/images/tabbar/subject_active.png",
            width: 30,
              gaplessPlayback: true
          ),
          label: "书影音"),
      BottomNavigationBarItem(
          icon: Image.asset(
            "asset/images/tabbar/group.png",
            width: 30,
              gaplessPlayback: true
          ),
          activeIcon: Image.asset(
            "asset/images/tabbar/group_active.png",
            width: 30,
              gaplessPlayback: true
          ),
          label: "小组"),
      BottomNavigationBarItem(
          icon: Image.asset(
            "asset/images/tabbar/mall.png",
            width: 30,
              gaplessPlayback: true
          ),
          activeIcon: Image.asset(
            "asset/images/tabbar/mall_active.png",
            width: 30,
              gaplessPlayback: true
          ),
          label: "市集"),
      BottomNavigationBarItem(
          icon: Image.asset(
            "asset/images/tabbar/profile.png",
            width: 30,
              gaplessPlayback: true
          ),
          activeIcon: Image.asset(
            "asset/images/tabbar/profile_active.png",
            width: 30,
              gaplessPlayback: true
          ),
          label: "我的"),
    ];
  }
}
