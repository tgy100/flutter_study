
import 'package:flutter/cupertino.dart';

class DataWidget extends InheritedWidget{

  int count;

  DataWidget({required Widget child,this.count = 0}) : super(child: child);

  static DataWidget of(BuildContext context) {

    return context.dependOnInheritedWidgetOfExactType()!;
  }

  @override
  bool updateShouldNotify(covariant DataWidget oldWidget) {
    // TODO: implement updateShouldNotify
    return oldWidget.count != count;
  }

}