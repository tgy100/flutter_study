

import 'package:flutter/cupertino.dart';
import 'package:flutter_demo03/douban/model/common/user_model.dart';

class UserProviderNotification extends ChangeNotifier{

  UserModel? _userModel;

  UserModel? getUserModel() {
    return _userModel;
  }

  set userModel(UserModel userModel) {

    _userModel = userModel;
    notifyListeners();
  }

}