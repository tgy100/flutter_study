import 'package:flutter/cupertino.dart';
import 'package:flutter_demo03/douban/model/home/home_item_model.dart';
import 'package:flutter_demo03/douban/page/home/home_content_item.dart';
import 'package:flutter_demo03/douban/service/home_request.dart';

class HomeContentStatefulWidget extends StatefulWidget {
  const HomeContentStatefulWidget({Key? key}) : super(key: key);

  @override
  _HomeContentStatefulWidgetState createState() =>
      _HomeContentStatefulWidgetState();
}

class _HomeContentStatefulWidgetState extends State<HomeContentStatefulWidget> {
  final List<HomeItemModel> _homeItemModels = <HomeItemModel>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Map<String, dynamic> params = Map();
    params["start"] = "0";
    params["count"] = "50";
    HomeRequest.requestHomeItem(params).then((value) {
      setState(() {
        _homeItemModels.addAll(value);
      });
    }).catchError((err) {});
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return HomeContentItemStatefulWidget(_homeItemModels[index], index);
      },
      itemCount: _homeItemModels.length,
    );
  }
}
