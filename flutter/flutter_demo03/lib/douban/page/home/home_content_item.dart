import 'package:flutter/material.dart';
import 'package:flutter_demo03/douban/model/home/home_item_model.dart';
import 'package:flutter_demo03/douban/widgets/star_widget.dart';

class HomeContentItemStatefulWidget extends StatefulWidget {
  final HomeItemModel homeItemModel;
  final int index;

  const HomeContentItemStatefulWidget(this.homeItemModel, this.index,
      {Key? key})
      : super(key: key);

  @override
  _HomeContentItemStatefulWidgetState createState() =>
      _HomeContentItemStatefulWidgetState();
}

class _HomeContentItemStatefulWidgetState
    extends State<HomeContentItemStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: const BoxDecoration(
          border:
              Border(bottom: BorderSide(width: 5, color: Color(0xffe2e2e2)))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          generalNumber(),
          const SizedBox(
            height: 5,
          ),
          generalCenterWidget(),
          const SizedBox(
            height: 5,
          ),
          generalBottonWidget()
        ],
      ),
    );
  }

  Widget generalNumber() {
    return Container(
      alignment: Alignment.center,
      width: 50,
      padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 5),
      decoration: BoxDecoration(
          color: Colors.yellow, borderRadius: BorderRadius.circular(3)),
      child: Text(
        "No${widget.index}",
        style: TextStyle(fontSize: 12),
      ),
    );
  }

  Widget generalCenterWidget() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [generalCenterImage(), generalCenterCenterColumn()],
    );
  }

  Widget generalCenterImage() {
    return Container(
      width: 80,
      height: 120,
      decoration: const BoxDecoration(
        color: Color.fromRGBO(240, 240, 240, 1),
      ),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Image.network(widget.homeItemModel.pic)),
    );
  }

  Widget generalCenterCenterColumn() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        generalCCenterTop(),
        generalCCenterStar()
      ],
    );
  }

  Widget generalCCenterTop() {
    return Text.rich(TextSpan(children: [
      const WidgetSpan(
          child: Icon(
            Icons.play_arrow,
            size: 26,
            color: Colors.red,
          ),
          alignment: PlaceholderAlignment.middle),
      ...widget.homeItemModel.title.runes.map((e) {
        return WidgetSpan(
            child: Text(
              String.fromCharCode(e),
              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            alignment: PlaceholderAlignment.middle);
      }).toList(),
      WidgetSpan(child: Text("(1994)",style: TextStyle(fontSize: 12),),alignment: PlaceholderAlignment.middle)
    ]));
    // return Row(
    //   crossAxisAlignment: CrossAxisAlignment.start,
    //   children: [
    //     Icon(
    //       Icons.play_arrow,
    //       size: 30,
    //       color: Colors.red,
    //     ),
    //     Text(
    //       widget.homeItemModel.title,
    //       style: TextStyle(fontSize: 18, color: Colors.black),
    //     )
    //   ],
    // );
  }

  Widget generalCCenterStar(){

    double rate = double.parse(widget.homeItemModel.ratingNum) / 10;

    return Row(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 5),
          child: StarStatefulWidget(ratio: rate,size: 20,),
        ),
        Text(widget.homeItemModel.ratingNum)
      ],
    );
  }

  Widget generalBottonWidget() {
    return Container(
      alignment: Alignment.center,
      height: 35,
      padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 5),
      decoration: BoxDecoration(
        color: const Color.fromRGBO(230, 230, 230, 1),
        borderRadius: BorderRadius.circular(3),
      ),
      child: Text(
        widget.homeItemModel.info,
        style: const TextStyle(fontSize: 16),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
