import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './home_content.dart';

class HomePageStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("首页"),
        backgroundColor: Colors.green,
      ),
      body: HomeContentStatefulWidget(),
    );
  }
}
