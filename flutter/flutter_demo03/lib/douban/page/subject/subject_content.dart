import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo03/douban/state/user_provider.dart';
import 'package:flutter_demo03/douban/widgets/star_widget.dart';
import 'package:provider/provider.dart';

class SubjectStatefulWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SubjectState();
  }
}

class _SubjectState extends State<SubjectStatefulWidget> {
  final List<String> contents = ["aaa", "bbb", "cccc"];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Stack(
      children: [
        Column(
          children: generalWidgets(),
        ),
        Positioned(
            right: 10,
            bottom: 10,
            child: Container(
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    contents.removeLast();
                  });
                },
                child: const Text("删除"),
              ),
            )),
        Positioned(
            left: 10,
            bottom: 10,
            child: Consumer(builder: (BuildContext context,
                UserProviderNotification value, Widget? child) {
              String val = "";

              if (value.getUserModel() != null) {
                val = value.getUserModel()!.userName;
              }

              return Text(val);
            })),
      ],
    );
  }

  List<Widget> generalWidgets() {
    return List.generate(contents.length, (index) {
      return KeyStatefulWidget(
        contents[index],
        key: ValueKey(contents[index]),
      );
    });
  }
}

class KeyStatefulWidget extends StatefulWidget {
  final String message;

  const KeyStatefulWidget(this.message, {Key? key}) : super(key: key);

  @override
  _KeyStatefulWidgetState createState() => _KeyStatefulWidgetState();
}

class _KeyStatefulWidgetState extends State<KeyStatefulWidget> {
  final Color backgroundColor = Color.fromRGBO(
      Random().nextInt(255), Random().nextInt(255), Random().nextInt(255), 1);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: double.infinity,
      child: Text(
        widget.message,
        style: TextStyle(fontSize: 20),
      ),
      color: backgroundColor,
    );
  }
}
