

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo03/douban/page/mall/mall_content.dart';

class MallPageStatelessWidget extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("书城"),
      ),
      body: MallContentWidget(),
    );
  }
}