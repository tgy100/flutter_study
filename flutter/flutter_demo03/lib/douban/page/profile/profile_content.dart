import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo03/douban/model/common/user_model.dart';
import 'package:flutter_demo03/douban/state/data_widget.dart';
import 'package:flutter_demo03/douban/state/user_provider.dart';
import 'package:provider/provider.dart';

class ProfileContentStatefulWidget extends StatefulWidget {
  const ProfileContentStatefulWidget({Key? key}) : super(key: key);

  @override
  _ProfileContentStatefulWidgetState createState() => _ProfileContentStatefulWidgetState();
}

class _ProfileContentStatefulWidgetState extends State<ProfileContentStatefulWidget> {

  int count = 0;

  
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: DataWidget(
        child: Column(
          children: [
            const InheritedDataWidget01(),
            const InheritedDataWidget02(),
            ElevatedButton(onPressed: (){
              setState(() {
                count++;
              });
            }, child: const Text("+1")),
            Consumer(builder: (cxt,UserProviderNotification userP,child){
              
              return ElevatedButton(onPressed: (){
                userP.userModel = UserModel("张三", "123123123");
              }, child: child);
            },child: Text("点我"),)
          ],
        ),
        count: count,
      ),
    );
  }
}


class InheritedDataWidget01 extends StatelessWidget{
  const InheritedDataWidget01({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    var dataWidget = DataWidget.of(context);

    return SizedBox(
      height: 40,
      width: double.infinity,
      child: Text("${dataWidget.count}"),
    );
  }

}

class InheritedDataWidget02 extends StatefulWidget {
  const InheritedDataWidget02({Key? key}) : super(key: key);

  @override
  _InheritedDataWidget02State createState() => _InheritedDataWidget02State();
}

class _InheritedDataWidget02State extends State<InheritedDataWidget02> {
  @override
  Widget build(BuildContext context) {

    var dataWidget = DataWidget.of(context);

    return SizedBox(
      height: 40,
      width: double.infinity,
      child: Text("${dataWidget.count}"),
    );
  }
}
