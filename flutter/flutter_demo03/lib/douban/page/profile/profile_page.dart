import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo03/douban/page/profile/profile_content.dart';

class ProfilePageStatelessWidget extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text("我的"),),
      body: ProfileContentStatefulWidget(),
    );
  }
}