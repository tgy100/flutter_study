import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StarStatefulWidget extends StatefulWidget {
  final int count;
  final double ratio;
  final double size;
  final Color normalColor;
  final Color highColor;

  const StarStatefulWidget({
        Key? key,
        this.count = 5,
        this.ratio = 1,
        this.size = 16,
        this.normalColor = Colors.grey,
        this.highColor = Colors.red
      })
      : super(key: key);

  @override
  _StarStatefulWidgetState createState() => _StarStatefulWidgetState();
}

class _StarStatefulWidgetState extends State<StarStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: generalStarList(),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: gerneralHighStarList(),
        )
      ],
    );
  }

  List<Widget> generalStarList() {
    return List.generate(widget.count, (index) {
      return Icon(
        Icons.star,
        size: widget.size,
        color: widget.normalColor,
      );
    }).toList();
  }

  List<Widget> gerneralHighStarList() {
    List<Widget> startList = <Widget>[];

    int allWidth = ((widget.size * widget.count) * widget.ratio).floor();

    int count =  (allWidth / widget.size).floor();

    double clipWidth = allWidth - count * widget.size;

    for(int i = 0; i < count;i++) {
      startList.add(Icon(
        Icons.star,
        size: widget.size,
        color: widget.highColor,
      ));
    }

    startList.add(ClipRect(
      child: Icon(
        Icons.star,
        size: widget.size,
        color: widget.highColor,
      ),
      clipper: StarCustomClipper(clipWidth),
    ));
    return startList;
  }
}

class StarCustomClipper extends CustomClipper<Rect> {
  final double startClip;

  StarCustomClipper(this.startClip);

  @override
  getClip(Size size) {
    // TODO: implement getClip
    return Rect.fromLTRB(0, 0, this.startClip, size.height);
  }

  @override
  bool shouldReclip(covariant StarCustomClipper oldClipper) {
    // TODO: implement shouldReclip
    return startClip != oldClipper.startClip;
  }
}
