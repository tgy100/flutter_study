/**
 * {
    "title": "肖申克的救赎",
    "url": "https://movie.douban.com/subject/1292052/",
    "pic": "https://img2.doubanio.com/view/photo/s_ratio_poster/public/p480747492.jpg",
    "rating_num": "9.7",
    "info": "导演:弗兰克·德拉邦特FrankDarabont主演:蒂姆·罗宾斯TimRobbins/...1994/美国/犯罪剧情"
    }
 */
class HomeItemModel {
  final String title;
  final String url;
  final String pic;
  final String ratingNum;
  final String info;
  final String year;

  HomeItemModel(this.title, this.url, this.pic, this.ratingNum, this.info,this.year);

  HomeItemModel.fromMap(Map<String, dynamic> itemMap)
      : this(
            itemMap["title"].toString(),
            itemMap["url"].toString(),
            itemMap["pic"].toString(),
            itemMap["rating_num"].toString(),
            itemMap["info"].toString(),"2000");

  static List<HomeItemModel> generalHomeItemModelsFromList(
      List<dynamic> items) {
    return List<HomeItemModel>.generate(items.length, (index) {
      return HomeItemModel.fromMap(items[index]);
    });
  }
}
