import 'package:flutter/material.dart';
import 'package:flutter_demo03/douban/main/main.dart';
import 'package:flutter_demo03/douban/service/home_request.dart';
import 'package:flutter_demo03/douban/state/user_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (ctx){
        return UserProviderNotification();
      })
    ],
    child: const MainStatelessWidget(),
  ));
}

class MainStatelessWidget extends StatelessWidget {
  const MainStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return MaterialApp(
      home: const MainStatefulWidget(),
      theme: ThemeData(
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
      ),
      debugShowCheckedModeBanner: false,
    );
    ;
  }
}
