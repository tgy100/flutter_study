import 'package:flutter_demo03/generated/json/base/json_convert_content.dart';
import 'package:flutter_demo03/day01/rank_entity.dart';

RankEntity $RankEntityFromJson(Map<String, dynamic> json) {
	final RankEntity rankEntity = RankEntity();
	final List<RankData>? data = jsonConvert.convertListNotNull<RankData>(json['data']);
	if (data != null) {
		rankEntity.data = data;
	}
	return rankEntity;
}

Map<String, dynamic> $RankEntityToJson(RankEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['data'] =  entity.data.map((v) => v.toJson()).toList();
	return data;
}

RankData $RankDataFromJson(Map<String, dynamic> json) {
	final RankData rankData = RankData();
	final String? titleName = jsonConvert.convert<String>(json['titleName']);
	if (titleName != null) {
		rankData.titleName = titleName;
	}
	final String? path = jsonConvert.convert<String>(json['path']);
	if (path != null) {
		rankData.path = path;
	}
	return rankData;
}

Map<String, dynamic> $RankDataToJson(RankData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['titleName'] = entity.titleName;
	data['path'] = entity.path;
	return data;
}