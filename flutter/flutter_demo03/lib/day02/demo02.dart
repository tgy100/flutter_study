import 'package:flutter/material.dart';

void main() {
  runApp(MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("demo01"),
        ),
        body: const HomeStatelessWidget(),
      ),
    );
  }
}

class HomeStatelessWidget extends StatelessWidget {
  final imagePath =
      "http://img0.dili360.com/ga/M01/48/3C/wKgBy1kj49qAMVd7ADKmuZ9jug8377.tub.jpg";

  const HomeStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: [
        Container(
          child: Image.network(imagePath),
        ),
        Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              height: 30,
              color: Color.fromRGBO(0, 0, 0, 0.5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:  [
                  Text("敖德萨多安达市多",style: TextStyle(color: Colors.white),),
                  IconButton(onPressed: (){
                    print("点击了");
                  }, icon: Icon(
                    Icons.pets,
                    size: 18,
                    color: Colors.white,
                  ))
                ],
              ),
            ))
      ],
    );
  }

  Widget demo01() {
    return Container(
      padding: const EdgeInsets.all(8),
      alignment: Alignment.center,
      width: 200,
      height: 200,
      color: Colors.blueAccent,
      child: Text("hello world" * 8),
    );
  }

  Widget demo02() {
    return Row(
      // crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.min,
      verticalDirection: VerticalDirection.down,
      children: [
        Container(
          width: 90,
          height: 130,
          color: Colors.green,
        ),
        Container(
          width: 70,
          height: 100,
          color: Colors.red,
        ),
        Container(
          width: 80,
          height: 150,
          color: Colors.red[50],
        ),
        Container(
          width: 90,
          height: 100,
          color: Colors.purple,
        ),
      ],
    );
  }
}
