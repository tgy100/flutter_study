import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: HomeStatefulWidget(),
      ),
    );
  }
}

class HomeStatefulWidget extends StatefulWidget {
  const HomeStatefulWidget({Key? key}) : super(key: key);

  @override
  _HomeStatefulWidgetState createState() => _HomeStatefulWidgetState();
}

class _HomeStatefulWidgetState extends State<HomeStatefulWidget> {
  final _scrollerController = ScrollController();

  final double _showTopButtonOffset = 300;

  bool _isShowTopButton = false;

  late bool _beforeShowTopButton;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _scrollerController.addListener(() {
      _beforeShowTopButton = _isShowTopButton;

      if (_scrollerController.offset > _showTopButtonOffset) {
        _isShowTopButton = true;
      } else {
        _isShowTopButton = false;
      }

      if (_beforeShowTopButton == _isShowTopButton) {
        return;
      }

      print("11111");
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return createShowWidget();
  }

  Widget createShowWidget() {
    if (_isShowTopButton) {
      return Stack(
        children: [createScrollView(), createTopWidget()],
      );
    }

    return Stack(
      children: [createScrollView()],
    );
  }

  Widget createTopWidget() {
    return Positioned(
        bottom: 20,
        right: 20,
        child: ElevatedButton(
          child: Icon(
            Icons.pets,
            size: 36,
          ),
          onPressed: () {
            _scrollerController.animateTo(0,
                duration: const Duration(milliseconds: 100),
                curve: Curves.easeIn);
          },
        ));
  }

  Widget createScrollView() {
    return NotificationListener(
        onNotification: (notification) {

          if (notification is ScrollStartNotification) {

            print("开始滚动");
          }else if (notification is ScrollUpdateNotification) {

            // 当前滚动的位置和总长度
            final currentPixel = notification.metrics.pixels;
            final totalPixel = notification.metrics.maxScrollExtent;

            print("正在滚动:${currentPixel}-${totalPixel}");
          }else if (notification is ScrollEndNotification) {

            print("结束滚动");
          }

          return true;
        },
        child: CustomScrollView(
          controller: _scrollerController,
          slivers: [
            const SliverAppBar(
              expandedHeight: 250,
              flexibleSpace: FlexibleSpaceBar(
                title: Text("哈哈"),
                background: Image(
                  image: NetworkImage(
                      "http://img0.dili360.com/ga/M01/48/3C/wKgBy1kj49qAMVd7ADKmuZ9jug8377.tub.jpg"),
                ),
              ),
            ),
            SliverSafeArea(
                top: false,
                sliver: SliverGrid(
                    delegate: SliverChildBuilderDelegate((context, index) {
                      return Container(
                        color: Color.fromRGBO(Random().nextInt(255),
                            Random().nextInt(255), Random().nextInt(255), 1),
                      );
                    }, childCount: 50),
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        childAspectRatio: 2))),
            SliverSafeArea(
              sliver: SliverFixedExtentList(
                  delegate: SliverChildBuilderDelegate((content, index) {
                    return Container(
                      color: Color.fromRGBO(Random().nextInt(255),
                          Random().nextInt(255), Random().nextInt(255), 1),
                    );
                  }, childCount: 20),
                  itemExtent: 100),
              top: false,
            )
          ],
        ));
  }
}

class HomeStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CustomScrollView(
      slivers: [
        const SliverAppBar(
          expandedHeight: 250,
          flexibleSpace: FlexibleSpaceBar(
            title: Text("哈哈"),
            background: Image(
              image: NetworkImage(
                  "http://img0.dili360.com/ga/M01/48/3C/wKgBy1kj49qAMVd7ADKmuZ9jug8377.tub.jpg"),
            ),
          ),
        ),
        SliverSafeArea(
            top: false,
            sliver: SliverGrid(
                delegate: SliverChildBuilderDelegate((context, index) {
                  return Container(
                    color: Color.fromRGBO(Random().nextInt(255),
                        Random().nextInt(255), Random().nextInt(255), 1),
                  );
                }, childCount: 50),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    childAspectRatio: 2))),
        SliverSafeArea(
          sliver: SliverFixedExtentList(
              delegate: SliverChildBuilderDelegate((content, index) {
                return Container(
                  color: Color.fromRGBO(Random().nextInt(255),
                      Random().nextInt(255), Random().nextInt(255), 1),
                );
              }, childCount: 20),
              itemExtent: 100),
          top: false,
        )
      ],
    );
  }

  void demo01() {
    SliverSafeArea(
        sliver: SliverPadding(
          padding: const EdgeInsets.all(10),
          sliver: SliverGrid(
              delegate: SliverChildBuilderDelegate((context, index) {
                return Container(
                  color: Color.fromRGBO(Random().nextInt(255),
                      Random().nextInt(255), Random().nextInt(255), 1),
                );
              }, childCount: 50),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                  childAspectRatio: 2)),
        ));
  }
}
