import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_demo03/day01/RankModel.dart';


void main() {

  runApp(MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("listView"),),
        body: JsonStatefulWidget(),
      ),
    );
  }
}


class JsonStatefulWidget extends StatefulWidget {
  const JsonStatefulWidget({Key? key}) : super(key: key);

  @override
  _JsonStatefulWidgetState createState() => _JsonStatefulWidgetState();
}

class _JsonStatefulWidgetState extends State<JsonStatefulWidget> {

  List<RankModel> rankes = <RankModel>[];

  final redSeparate = const Divider(color: Colors.red,);
  final blueSeparate = const Divider(color: Colors.blue,);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    rootBundle.loadString("asset/jsons/data01.json").then((value) {

      Map<String,dynamic> data = json.decode(value);
      List<dynamic> lastDatas = data["data"];

      for(var lastData in lastDatas) {

        rankes.add(RankModel.fromMap(lastData));
      }

      setState(() {

      });
    });
  }


  @override
  Widget build(BuildContext context) {

    // return ListView(
    //   children: generalWidget(),
    // );
    // return ListView.builder(itemBuilder: (BuildContext context, int index){
    //
    //   return ListTile(
    //     title: Text(rankes[index].name),
    //     subtitle: Text(rankes[index].path),
    //   );
    // },
    //   itemCount: rankes.length,
    //   itemExtent: 100,
    // );
    return ListView.separated(itemBuilder: (BuildContext context,int index){

      return ListTile(
        title: Text(rankes[index].name),
        subtitle: Text(rankes[index].path),
      );
    }, separatorBuilder: (BuildContext context, int index){

      return index % 2 == 0 ? redSeparate:blueSeparate;

    }, itemCount: rankes.length);
  }

  List<Widget> generalWidget() {

    List<Widget> result = <Widget>[];

    /**
     * ListTile(
        leading: Icon(Icons.email, size: 36,),
        title: Text("邮箱"),
        subtitle: Text("邮箱地址信息"),
        trailing: Icon(Icons.arrow_forward_ios),
        ),
     */

    rankes.forEach((element) {

      result.add(ListTile(
        leading: Icon(Icons.email,size: 36,),
        title: Text(element.name),
        subtitle: Text(element.path),
        trailing: Icon(Icons.arrow_forward_ios),
      ));
    });

    return result;
  }

}

