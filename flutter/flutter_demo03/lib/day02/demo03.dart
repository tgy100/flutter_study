import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("listView"),
        ),
        body: HomeStatelessWidget(),
      ),
    );
  }
}

class HomeStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return GridView.builder(
        padding: EdgeInsets.all(10),
        itemCount: 100,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            crossAxisSpacing: 10,
            mainAxisSpacing:  10,
            childAspectRatio: 1),
        itemBuilder: (content, index) {
          return Container(
            color: Color.fromRGBO(Random().nextInt(255), Random().nextInt(255),
                Random().nextInt(255), 1),
          );
        });
  }

  Widget demo01() {
    return ListView(
      scrollDirection: Axis.horizontal,
      itemExtent: 180,
      children: [
        Container(
          // width: 100,
          height: 80,
          color: Colors.red,
        ),
        Container(
          width: 100,
          height: 120,
          color: Colors.yellow,
        ),
        Container(
          width: 100,
          height: 100,
          color: Colors.purple,
        ),
        Container(
          width: 100,
          height: 150,
          color: Colors.pink,
        ),
      ],
    );
  }

  Widget demo02() {
    return ListView.separated(
        itemBuilder: (content, index) {
          return Text("asd-${index}");
        },
        separatorBuilder: (context, index) {
          return Divider(
            color: Colors.purple,
            indent: 10,
            endIndent: 10,
            thickness: 4,
          );
        },
        itemCount: 50);
  }
}
