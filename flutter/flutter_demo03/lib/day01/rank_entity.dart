import 'dart:convert';
import 'package:flutter_demo03/generated/json/base/json_field.dart';
import 'package:flutter_demo03/generated/json/rank_entity.g.dart';

@JsonSerializable()
class RankEntity {

	late List<RankData> data;
  
  RankEntity();

  factory RankEntity.fromJson(Map<String, dynamic> json) => $RankEntityFromJson(json);

  Map<String, dynamic> toJson() => $RankEntityToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class RankData {

	late String titleName;
	late String path;
  
  RankData();

  factory RankData.fromJson(Map<String, dynamic> json) => $RankDataFromJson(json);

  Map<String, dynamic> toJson() => $RankDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}