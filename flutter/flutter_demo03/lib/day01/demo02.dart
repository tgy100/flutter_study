import 'package:flutter/material.dart';


void main() {
  runApp(const MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {

  const MainStatelessWidget({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("demo02"),
        ),
        body: const HomeStatefullWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {

          },
        ),
      ),
    );
  }
}

class HomeStatefullWidget extends StatefulWidget{
  const HomeStatefullWidget({Key? key}) : super(key: key);


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeState();
  }

}

class _HomeState extends State<HomeStatefullWidget> {

  int index = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("init state");
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(onPressed: (){

                setState(() {
                  index++;
                });

              }, child: Text("+1")),
              ElevatedButton(onPressed: (){
                setState(() {
                  index--;
                });
              }, child: Text("-1")),
            ],
          ),
          Text("计数器${index}",style: TextStyle(fontSize: 18),),
          CustomStatefullWidget()
        ],
      ),
    );
  }
}


class CustomStatefullWidget extends StatefulWidget {
  const CustomStatefullWidget({Key? key}) : super(key: key);

  @override
  _CustomStatefullWidgetState createState() => _CustomStatefullWidgetState();
}

class _CustomStatefullWidgetState extends State<CustomStatefullWidget> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print("_CustomStatefullWidgetState initState");
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    print("_CustomStatefullWidgetState _CustomStatefullWidgetState");
  }

  @override
  void didUpdateWidget(covariant CustomStatefullWidget oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    print("_CustomStatefullWidgetState didUpdateWidget");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text("custom"),
    );
  }
}


class HomeStatelessWidget01 extends StatelessWidget {
  const HomeStatelessWidget01({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(onPressed: (){

              }, child: const Text("+1")),
              ElevatedButton(onPressed: (){

              }, child: const Text("-1")),
            ],
          ),
          SizedBox(height: 10,),
          Text("当前记数"),
        ],
      ),
    );
  }
}

