import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("row column"),
        ),
        body: const RowStatelessWidget(),
      ),
    );
  }
}

class RowStatelessWidget extends StatelessWidget {
  const RowStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Row(
      // mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(child: Container(
          // width: 60,
          height: 100,
          color: Colors.blueAccent,
        ),flex: 3,),
        Container(
          width: 80,
          height: 150,
          color: Colors.red,
        ),
        Container(
          width: 90,
          height: 150,
          color: Colors.purple,
        ),
        Expanded(child:  Container(
          width: 100,
          height: 200,
          color: Colors.green,
        ),flex: 1,),
      ],
    );
  }
}
