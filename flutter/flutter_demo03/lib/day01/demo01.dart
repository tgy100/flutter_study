import 'package:flutter/material.dart';

void main() {
  runApp(const TGYMainStatelessWidget());
}

class TGYMainStatelessWidget extends StatelessWidget {

  const TGYMainStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("demo01"),
        ),
        body: const TGYHomeStatelessWidget(),
      ),
    );
  }
}

class TGYHomeStatelessWidget extends StatelessWidget {

  const TGYHomeStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Text(
          "hello world" * 8,
          textAlign: TextAlign.center,
          style: const TextStyle(fontSize: 18,color: Colors.red),
        )
      ],
    );
  }
}
