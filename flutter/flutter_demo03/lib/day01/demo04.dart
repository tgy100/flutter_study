import 'package:flutter/material.dart';



void main() {

  runApp(const MainStatelessWidget());
}


class MainStatelessWidget extends StatelessWidget {

  const MainStatelessWidget({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("表单"),
        ),
        body: const HomeStatelessWidget(),
      ),
    );
  }

}


class HomeStatelessWidget extends StatelessWidget {

  const HomeStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        FormStatefulWidget(),
      ],
    );
  }
}

class FormStatefulWidget extends StatefulWidget {
  const FormStatefulWidget({Key? key}) : super(key: key);

  @override
  _FormStatefulWidgetState createState() => _FormStatefulWidgetState();
}

class _FormStatefulWidgetState extends State<FormStatefulWidget> {

  final textEditingController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    textEditingController.text = "hello world";

    textEditingController.addListener(() {

      print("textEditingController:${textEditingController.text}");
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: textEditingController,
      decoration: InputDecoration(
          icon: Icon(Icons.people),
          // labelText: "username",
          // labelStyle: TextStyle(color: Colors.pink),
          hintText: "请输入用户名",
          border: InputBorder.none,
          filled: true,
          fillColor: Colors.blue
      ),
      onChanged: (val) {

        print("onChanged ${val}");
      },
      onSubmitted: (val){
        print("onSubmitted ${val}");
      },
    );
  }
}




