
class RankModel {
  String name;
  String path;

  RankModel(this.name, this.path);

  RankModel.fromMap(Map<String,dynamic> rank):this(rank["titleName"].toString(),rank["path"].toString());

}
