import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {
  const MainStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("表单"),
        ),
        body: const HomeStatelessWidget(),
      ),
    );
  }
}

class HomeStatelessWidget extends StatelessWidget {
  const HomeStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return const Center(
      child: FormStatefulWidget(),
    );
  }
}

class FormStatefulWidget extends StatefulWidget {
  const FormStatefulWidget({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FormState();
  }
}

class _FormState extends State<FormStatefulWidget> {

  String? name;
  String? password;

  // final registerFormKey = GlobalKey<_FormState>();
  final registerFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  void submit() {

    // registerFormKey.currentState.save();
    // registerFormKey.currentContext.s
    // registerFormKey.currentState.save();

    // registerFormKey.currentState!.save();

    var currentState = registerFormKey.currentState!;

    currentState.save();

    print(name!);
    print(password!);
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Form(
        key: registerFormKey,
        child: Column(
          children: [
            TextFormField(
              decoration:
              const InputDecoration(icon: Icon(Icons.people), labelText: "用户名或者手机号"),
              onSaved: (val) {

                name = val;
              },
            ),
            TextFormField(
              obscureText: true,
              decoration: const InputDecoration(icon: Icon(Icons.lock), labelText: "密码"),
              onSaved: (val){
                password = val;
              },
            ),
            ElevatedButton(onPressed: (){
              submit();
            }, child: Text("登录"))
          ],
        ));
  }
}
