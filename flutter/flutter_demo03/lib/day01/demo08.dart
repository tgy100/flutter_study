import 'package:flutter/material.dart';

void main() {
  runApp(MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("stack 布局"),),
        body: HomeStatelessWidget(),
      ),
    );
  }
}

class HomeStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Stack(
        children: [
          Container(
            width: 200,
            height: 200,
            color: Colors.green,
          ),
          const Positioned(
              left: 10,
              top: 10,
              child: Icon(
                Icons.pets,
                size: 36,
              )),
          const Positioned(
              bottom: 0,
              right: 0,
              child: Text(
                "呵呵呵",
                style: TextStyle(fontSize: 20),
              ))
        ],
      ),
    );
  }
}
