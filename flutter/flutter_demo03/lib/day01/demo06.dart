import 'package:flutter/material.dart';

void main() {
  runApp(const MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {
  const MainStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("布局"),
        ),
        // body: HomeStatelessWidget(),
        body: ContainerStatelessWidget(),
      ),
    );
  }
}

class ContainerStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Center(child: container03());
  }

  Widget container01() {
    return Container(
      width: 200,
      height: 200,
      color: Colors.pink,
      child: Icon(
        Icons.pets,
        size: 36,
      ),
    );
  }

  Widget container02() {
    return Container(
      width: 200,
      height: 200,
      decoration: BoxDecoration(
        color: Colors.blue,
        border: Border.all(width: 2, color: Colors.yellow),
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(offset: Offset(5, 5), color: Colors.grey, blurRadius: 5)
        ],
        gradient: LinearGradient(colors: [Colors.blueAccent, Colors.purple]),
      ),
      child: Icon(Icons.pets, size: 36),
    );
  }

  Widget container03(){

    return Container(
      width: 200,
      height: 200,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          image:DecorationImage(image: NetworkImage("https://tva1.sinaimg.cn/large/006y8mN6gy1g7aa03bmfpj3069069mx8.jpg"))
      ),
    );
  }
}

class PaddingStatelessWidget extends StatelessWidget {
  const PaddingStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Text(
        "asdasd" * 8,
        style: TextStyle(fontSize: 18),
      ),
    );
  }
}

class HomeAlignStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    // return const Align(
    //   child: Icon(
    //     Icons.pets,
    //     size: 36,
    //   ),
    //   heightFactor: 3,
    //   widthFactor: 3,
    //   alignment: Alignment.topCenter,
    // );
    return Center(
      child: Icon(
        Icons.pets,
        size: 36,
      ),
      // widthFactor: 3,
      // heightFactor: 3,
    );
  }
}
