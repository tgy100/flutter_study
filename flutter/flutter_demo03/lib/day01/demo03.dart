import 'package:flutter/material.dart';

void main() {
  runApp(const MainStatelessWidget());
}

class MainStatelessWidget extends StatelessWidget {
  const MainStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("demo02"),
        ),
        body: HomeStatefullWidget(),
      ),
    );
  }
}

class HomeStatefullWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeState();
  }
}

class _HomeState extends State<HomeStatefullWidget> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: ListView(

        children: [
          Text(
            "hello world" * 8,
            textAlign: TextAlign.center,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 18),
          ),
          Text.rich(TextSpan(children: [
            TextSpan(
                text: "😄🍺",
                style: TextStyle(fontSize: 18, color: Colors.red)),
            TextSpan(
                text: "hello" * 2,
                style: TextStyle(fontSize: 20, color: Colors.yellow)),
            TextSpan(
                text: "屁的sad",
                style: TextStyle(fontSize: 25, color: Colors.blueAccent)),
          ])),
          TextButton(onPressed: () {}, child: Text("确定")),
          OutlinedButton(
            onPressed: () {},
            child: Text("提交"),
            style: ButtonStyle(
              side: MaterialStateProperty.all(
                  BorderSide(width: 2, color: Colors.red)),
              // elevation: MaterialStateProperty.all(10)
            ),
          ),
          OutlinedButton.icon(
              onPressed: () {}, icon: Icon(Icons.add), label: Text("确定")),
          Container(
            width: 200,
            height: 45,
            // padding: EdgeInsets.only(left: 16,right: 16,top: 10,bottom: 10),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                border: Border.all(color: Color(0xff5f6fff), width: 1),
                color: Colors.white,
                borderRadius: BorderRadius.circular((10.0))),
            child: Text('登录',
                style: TextStyle(color: Color(0xff5f6fff), fontSize: 14)),
          ),
          TextButton(
              style: ButtonStyle(
                //圆角
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.67))),
                  //边框
                  side: MaterialStateProperty.all(
                    BorderSide(color: Colors.red, width: 0.67),
                  ),
                  //背景
                  backgroundColor:
                  MaterialStateProperty.all(Colors.transparent)),
              child: Text(
                '确定',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.yellow,
                    fontSize: 14),
              ),
              onPressed: () {}),
          Container(
            width: 300,
            height: 300,
            decoration: BoxDecoration(
              color: Colors.red,
            ),
            // color: Colors.yellow,
            child: Image.network(
              "http://img0.dili360.com/ga/M01/48/3C/wKgBy1kj49qAMVd7ADKmuZ9jug8377.tub.jpg",
              alignment: Alignment.bottomRight,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            width: 300,
            height: 300,
            decoration: BoxDecoration(
                color: Colors.purple
            ),
            child: Image.asset("asset/images/LaunchImage.png",fit: BoxFit.cover,),
          ),
          Container(
            width: 300,
            height: 300,
            child: CircleAvatar(
              radius: 150,
              backgroundImage: NetworkImage("http://img0.dili360.com/ga/M01/48/3C/wKgBy1kj49qAMVd7ADKmuZ9jug8377.tub.jpg"),
              child: Container(
                width: 100,
                height: 100,
                alignment: Alignment(0,3),
                child: Text("hello world"),
              ),
            ),
          ),
          Container(

            child: ClipOval(
              child: Image.network("http://img0.dili360.com/ga/M01/48/3C/wKgBy1kj49qAMVd7ADKmuZ9jug8377.tub.jpg",width: 200,height: 200,),
            ),
          ),
          Container(
            width: 200,
            height: 200,
            decoration: BoxDecoration(
                color: Colors.red
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Image.network("http://img0.dili360.com/ga/M01/48/3C/wKgBy1kj49qAMVd7ADKmuZ9jug8377.tub.jpg",width: 100,height: 100,fit: BoxFit.fitWidth,),
            ),
          )
        ],
      ),
    );
  }
}
