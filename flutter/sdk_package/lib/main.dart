import 'package:flutter/material.dart';
import './game_widget.dart';

void main(){

  runApp(HomeWidget());
}


class HomeWidget extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("首页"),
        ),
        body: ListView.builder(itemBuilder: (context,index){
          return GestureDetector(
            child: ListTile(
              leading: Icon(Icons.pets,size: 30,color: Colors.red,),
              title: Text("index ${index}"),
              subtitle: Text("哈哈"),
            ),
            onTap: (){
                
              Navigator.of(context).push(MaterialPageRoute(builder: (context){

                return GameWidget("${index}");
              }));
            },
          );
        },itemCount: 100,),
      ),
    );
  }

}