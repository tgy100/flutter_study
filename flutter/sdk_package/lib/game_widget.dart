import 'package:flutter/material.dart';

class GameWidget extends StatefulWidget {

  String channelId;

  GameWidget(this.channelId,{Key? key}) : super(key: key);

  @override
  _GameWidgetState createState() => _GameWidgetState();
}

class _GameWidgetState extends State<GameWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("游戏列表"),
      ),
      body: Center(
        child: Text("${widget.channelId}"),
      ),
    );
  }
}
