import 'package:flutter/material.dart';

void main() {
  runApp(const TGYMainWidget());
}

class TGYMainWidget extends StatelessWidget {
  const TGYMainWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: Text(
              "demo01",
              style: TextStyle(fontSize: 18),
            ),
          ),
          body: TGYHomeWidget(),
        ));
  }
}

class TGYHomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Text(
        "hello world",
        style: TextStyle(
          color: Colors.yellow,
          fontSize: 36,
        ),
      ),
    );
  }
}
