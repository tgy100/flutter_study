import 'package:flutter/material.dart';

void main() {
  runApp(const TGYMainWidget());
}

class TGYMainWidget extends StatelessWidget {
  const TGYMainWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("demo01"),
        ),
        body: TGYHomeStatefulWidget(),
      ),
    );
  }
}

class TGYHomeStatefulWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TGYHomeWidgetState();
  }
}

class _TGYHomeWidgetState extends State {
  bool checkFlag = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Checkbox(
              value: checkFlag,
              onChanged: (value) {
                setState(() {
                  checkFlag = !checkFlag;
                });
              }),
          const Text(
            "同意协议",
            style: TextStyle(
              fontSize: 18,
              backgroundColor: Colors.green,
            ),
          )
        ],
      ),
    );
  }
}
