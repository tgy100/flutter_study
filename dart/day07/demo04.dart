class Animal {
  String name;
  static Map<String, Animal> container = Map();

  String get getName {
    return name;
  }

  set setName(String name) {
    this.name = name;
  }

  Animal(this.name);

  static Animal getAnimal(String name) {
    if (container.containsKey(name)) {
      return container[name]!;
    }

    final animal = Animal(name);
    container[name] = animal;

    return animal;
  }

  @override
  String toString() {
    // TODO: implement toString
    return "name ${name}";
  }
}

void test01() {
  var a1 = Animal.getAnimal("张三");
  var a2 = Animal.getAnimal("张三");

  print(identical(a1, a2));
}

void test02() {
  var a1 = Animal.getAnimal("张三");
  print(a1.getName);
  a1.setName = "李四";
  print(a1.getName);
}

void main(List<String> args) {
  test02();
}
