void test01() {
  var num = 10;
  print(num / 2);
  print(num % 2);

  String? age = null;

  // age ??= "2222";
  // print(age);

  var opt = age ?? "122";

  print(opt);
}

class Person {
  String name;

  Person(this.name);

  void eat() {
    print("eat:${name}");
  }

  void run() {
    print("run:${name}");
  }
}

void test02() {
  var names = ["张三", "李四", "王五"];

  for (var name in names) {
    print(name);
  }
  print("${names.runtimeType}");

  for (var i = 0; i < names.length; i++) {
    print(names[i]);
  }
}

void main(List<String> args) {
  // test01();
  // var per = Person("12")
  //   ..eat()
  //   ..run();
  test02();
}
