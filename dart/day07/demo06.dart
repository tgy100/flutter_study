abstract class A {
  run();
}

abstract class B {
  eat() {
    print("111");
  }
}

class Animal implements A, B {
  @override
  run() {
    // TODO: implement run
    throw UnimplementedError();
  }

  @override
  eat() {
    // TODO: implement eat
    throw UnimplementedError();
  }
}

mixin C {
  fly() {
    print("111");
  }
}

mixin D {
  opt() {
    print("111");
  }
}

class Person with C, D {}

enum Colors { RED, YELLOW }

void test03() {
  var per = Person();
  per.fly();
  print(Colors.RED.index);
}

void test04() {
  print(Colors.values);
}

void main(List<String> args) {
  test04();
}
