class Animal {
  String name;
  Animal(this.name);

  String get getName {
    return this.name;
  }

  set setName(String name) {
    this.name = name;
  }

  void say() {
    print("name:${name}");
  }
}

class Person extends Animal {
  Person(String name) : super(name);

  @override
  void say() {
    // TODO: implement say
    super.say();
    print("person:${name}");
  }
}

void test01() {
  var per = Person("李四");
  print(per.name);
  per.say();
}

abstract class Shape {
  double getArea();
}

class Circle extends Shape {
  @override
  getArea() {
    // TODO: implement getArea
    return 3.1;
  }
}

void main(List<String> args) {
  test01();
}
