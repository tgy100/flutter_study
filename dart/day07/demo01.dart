void test01() {
  print("1111");

  String name = "111";
  int age = 20;

  print("${name},${age}");
}

void test02() {
  var name = "12";
  print(name);
}

void test03() {
  dynamic name = "123";
  print(name.runtimeType);
  name = 21;
  print(name.runtimeType);
}

class Person {
  const Person();
}

void test04() {
  const p1 = Person();
  const p2 = Person();

  print(identical(p1, p2));
}

int sum(a, b) {
  return a + b;
}

void test05() {
  final p = sum(10, 20);
  print(p);
  // const q = sum(1, 2);
  // print(q);
}

void test06() {
  var p = "12";
  var result = int.parse(p);

  print(result.toString());
}

void test07() {
  String content = '''asdasd
  opt
  name
  ''';

  print(content);
}

void test08() {
  List<int> ages = [12, 22, 31];

  ages.sort((a, b) => a - b);
  print(ages);

  Map<String, Object> person = {"name": '张三', "age": 21};

  print("${person.runtimeType}");
  print(person.values);
  print(person.keys);
}

void test09(String name, [int a = 0, int b = 0, int c = 11]) {
  print(name);
  print(a);
  print(b);
  print(c);
}

void test10(String name, {required int? heigt, int? width}) {
  print(name);
  print(heigt);
  print(width);
}

void test11([int? a, int b = 10]) {
  print(a);
  print(b);
}

void test12() {
  List<String> names = ["zhangsan", "lisi", "王五"];

  names.forEach((elemat) => print(elemat));
}

Function addFun(int add) {
  return (int a) {
    return add + a;
  };
}

typedef CallBack = int Function(int a, int b);

void test13(CallBack callBack) {
  var result = callBack(19, 1);
  print(result);
}

void main(List<String> args) {
  // test08();
  // test09("name", 1, 2);
  // test10("122", heigt: 21);
  // test11(1, 2);
  // test10("111", heigt: 21);
  // test12();
  // var fun = addFun(10);
  // print(fun(1));
  test13((a, b) => a + b);
}
