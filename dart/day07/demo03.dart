class Person {
  String name;
  int age;

  Person(String name, int age)
      : this.age = age,
        this.name = name {
    print("121123");
  }

  Person.fromMap(Map<String, Object> maps)
      : this(maps["name"] as String, maps["age"] as int);

  @override
  String toString() {
    // TODO: implement toString
    return "${name}-${age}";
  }
}

void test03() {
  var per = Person("张三", 21);
  print(per);
}

void test04() {
  Map<String, Object> maps = {"name": "张三", "age": 21};
  final per = Person.fromMap(maps);
  print(per);
}

class Animal {
  final String name;
  final int age;

  const Animal(this.name, this.age);
}

void test05() {
  const per = const Animal("赵六", 10);
  const per01 = const Animal("赵六", 10);

  print(identical(per, per01));
}

void main(List<String> args) {
  test05();
}
