
void main(List<String> args) {

  final myClass = MyClass('tony');
  myClass.eat();
}


class RestrictClass {

  String name;
  RestrictClass(this.name);

}

mixin MyMixin on RestrictClass{

  void eat() {
    print("eat $name");
  }
}

class MyClass extends RestrictClass with MyMixin{

  MyClass(String name) : super(name);

}
