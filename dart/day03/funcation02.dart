

import '../day01/demo01.dart';

void main(List<String> args) {

  final add = generalAdd(10);

  final res = add(10, 2);
  print(res);

  // print(test());
  // assert(test01() == null);

  final t = test01()?.toString();
  print(t);
  // assert(false,"111");

  try {

    exception(1);
  }on Exception catch(e,stack) {

    print(e);
    print(stack);
    rethrow;
  }finally {
    print("1111");
  }

  print("1111");
}



int? test01() {

  return null;
}


Function generalAdd(int base) {

  return (int a, int b) {

    return base + a + b;
  };
}

void exception(int a) {

  if (a == 1) {

    // throw "a == 1";
    throw MyException(200, "成功");
  }
}

class MyException implements Exception {

  int code;
  String message;

  MyException(this.code,this.message);

  @override
  String toString() {
    // TODO: implement toString
    return "code:$code,message:$message";
  }
}