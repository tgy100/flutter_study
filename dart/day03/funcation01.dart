

final myMap = Map<String,String>();

void main(List<String> args) {

  final result = isNode("111");
  print(result);

  final sumRst = sum(a: 10, b: 20);
  print(sumRst);

  doStuff();

  final names = ['张三','李四'];

  names.forEach((element) => print(element));
}


// bool isNode(String key) {
//
//   return myMap[key] != null;
// }

bool isNode(String key) => myMap[key] != null;

int sum({int a = 0, int b = 0}) {

  return a + b;
}

void doStuff([List<int> count = const [1,2,3]]) {

    print(count);
}