
void main(List<String> args) {

  var names = const [
    "张三",
    "李四",
    "王五",
  ];

  var names01 = const [
    "张三",
    "李四",
    "王五",
  ];
  print(identical(names01, names));
  

  // const list = "names $names";
  names = ["赵六",...names];
  print(names);

  var lis;

  names = ['opt',...?lis];

  print(names);



  final opts = [1,for(int i =0; i < 3;i++)'aaa:${i}'];
  print(opts);

  // 因为 map先有的，所以 {} 默认是创建一个 map<dynamic,dynamic> 类型的变量
  var mySet = {'111'};

  print(mySet.runtimeType);

  mySet.add("23");
  print(mySet);

  final myMap = Map<int,String>();

  myMap[1] = "111";
  myMap[2] = "222";

  print(myMap);

  assert(myMap[3] != null);
}