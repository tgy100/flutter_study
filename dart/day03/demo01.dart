void main(List<String> args) {
  final person = Person();
  person("张三");

  final Object i = 3;

  final nums = [i as int];
  final nums01 = [i];
  print(nums);
  print(nums.runtimeType);
  print(nums01.runtimeType);

  final names = ['张三', '李四'];

  final mapp = {...names};
  print(mapp);
}

class Person {
  /**
   * 类的实例不能使用 const 修饰，可以使用 final，类变量可以使用const
   */
  static const String name = "stack";
  final int age;
  Person() : this.age = 10;

  void call(String name) {
    print("name $name");
  }
}
