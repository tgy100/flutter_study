
import 'dart:math' as math;

void main(List<String> args) {
  int num = 10;
  double pi = 3.141592653589793;
  print(pi.ceil());
  print(pi.floor());
  print(math.pi);

  // 字符串转 int double
  print(int.parse("111"));
  final api = double.parse("3.141592653589793");
  // double to string
  print("api : ${api.toString()}");
  print("api : ${api.toStringAsFixed(3)}");

  final na01 = "李四";
  final na02 = "李四";

  print(identical(na01, na02));
  print(na01 == na02);

  final per01 = Person("张三", 21);
  final per02 = Person("张三", 21);

  print(per01 == per02);
  print(identical(per01, per02));

  print(na01 + na02 + api.toString());
}


class Person {

  String name;
  int age;

  Person(this.name,this.age);

  @override
  bool operator ==(Object other) {
    // TODO: implement ==
    if (identical(this, other)) {

      return true;
    }

    if (!(other is Person)) {
      return false;
    }

    final otherPerson = other as Person;
    return age == otherPerson.age && name == otherPerson.name;
  }
}