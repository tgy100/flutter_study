import 'package:dio/dio.dart';
import 'http_config.dart';

class HttpRequest {
  static final Dio dio = Dio(
    BaseOptions(
        baseUrl: HttpConfig.baseURL, connectTimeout: HttpConfig.timeOut),
  );

  static Future request(String url,
      {String method = 'get',
      Map<String, dynamic>? params,
      Interceptor? outInter}) async {
    final options = Options(method: method);

    final inter = InterceptorsWrapper(
        onError: (DioError e, ErrorInterceptorHandler handler) {
      handler.next(e);
    }, onRequest: (
      RequestOptions options,
      RequestInterceptorHandler handler,
    ) {
      handler.next(options);
    }, onResponse: (
      Response e,
      ResponseInterceptorHandler handler,
    ) {
      handler.next(e);
    });

    List<Interceptor> inters = [inter];

    if (outInter != null) {
      inters.add(outInter);
    }

    dio.interceptors.addAll(inters);

    try {
      final respone =
          await dio.request(url, queryParameters: params, options: options);

      return respone.data;
    } on Exception catch (e) {
      return Future.value(e);
    }
  }
}

void main(List<String> args) {
  HttpRequest.request("get!", params: {"name": "zhangsan"}).then((value) {
    print(value);
  });
}
