late String name = getName();

String getName() {
  print("返回值");
  return "hello world";
}

void test01() {
  int? age;
  print("111");
  // print(age!);

  age = 20;
  int opt = age;

  // print(opt);
  print(name);
  var opt01 = 111;
}

void test02() {
  const List<int> opt = [1, 3, 4];

  // opt.add(2);
  print(opt);
  final List<int> tony = [1, 2, 3];
  tony.add(5);
  print(tony);

  var top01 = [...tony];
}

void test03() {
  var content = r"asda\ndasd";
  print(content);

  var list;
  print(list);
  var opt = [1, if (list is List && list != null) ...list];
  print(opt);

  var tony = ["1123", for (var i = 0; i < 5; i++) i.toString()];
  print(tony);
}

void main(List<String> args) {
  test03();
}
