import 'dart:io';

Future<String> getData() {
  return Future(() {
    sleep(Duration(seconds: 3));

    throw Exception("赫赫巍巍·");

    return "hello world";
  });
}

void test01() {
  getData().then((value) {
    print(value);
  }).catchError((error) {
    print(error);
  });
}

void test02() {
  Future.delayed(Duration(seconds: 3), () {
    print("执行完成");
    return "12132";
  }).then((value) => print(value));
}

Future<String> test03() async {
  var result = await Future.delayed(Duration(seconds: 3), () {
    return "1123";
  });

  return result;
}

void main(List<String> args) {
  // test01();
  // test02();
  test03().then((value) => print(value));
}
