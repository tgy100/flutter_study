typedef Compare = int Function(int, int);

void test01() {
  final ages = [3, 1, 2, 4, 6, 5];

  ages.sort((int a, int b) {
    return a - b;
  });

  print(ages);
}

void test02(Compare compare) {
  int a = 10;
  final b = 20;

  final result = compare(a, b);
  print(result);
}

void main(List<String> args) {
  test01();
}
