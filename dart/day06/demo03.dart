class Person {
  late String name;
  late int age;

  Person(this.name, this.age);

  Person.fromMap(Map<String, dynamic> map)
      : this.name = map['name'],
        this.age = map['age'];

  void run() {
    print("1111");
  }

  @override
  String toString() {
    // TODO: implement toString
    return 'name:${name},age:${age}';
  }
}

class Animal {
  final String name;
  final int age;

  const Animal(this.name, this.age);
}

void main(List<String> args) {
  test02();
}

void test([int? age]) {
  print(age);
}

void test01() {
  Person? person;

  person = Person("zhangsan", 21);
  person.run();

  final per1 = Person.fromMap({"name": 'lisi', 'age': 21});

  print(per1);
}

void test02() {
  const anaiml01 = Animal("11", 21);
  print(anaiml01);

  const anaiml02 = Animal("11", 21);
  print(anaiml02);

  print(anaiml01 == anaiml02);
}
