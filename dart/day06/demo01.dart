late int age = getAge();

int getAge() {
  print('getAge call');
  return 10;
}

void test01() {
  int opt = 10;

  final myAge = opt * 10;

  // const my01 = [];

  // my01.add(111);

  final ages = [1, 2, 4];

  final opts = [if (ages is List<double>) ...ages];

  print(opts);

  // print(opts);

  // print(age);
  final pInt = int.parse('111');
  print(pInt);
  print(pInt.toString());
  final score = 3.1415926;
  print(score.toStringAsFixed(4));
}

void test02() {
  final content = r'option \n asdadasd';
  print(content);
}

void test03() {
  var ages = [1, 3, 4, 5];

  List<int>? agesNull;

  final all = [6, ...ages];
  print(all);

  final all02 = [6, ...?agesNull];

  print(all02);

  final all03 = [
    "1",
    for (int k in ages)
      if (k > 3) "#${k}"
  ];

  print(all03);
}

void run([int age = 0, int score = 0]) {}

void run01({required int age, required double score}) {}

void main(List<String> argv) {
  test03();
}
