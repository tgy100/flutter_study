void main(List<String> args) {
  final a = 10;
  final b = 20;
  print(sum(a, b));

  final c = opt(a, b, 11, 12);
  print(c);

  test01(11, name: "11");
}

sum(int a, int b) => a + b;

int opt(int a, int b, [int c = 0, int d = 0]) {
  return a + b + c + d;
}

int test01(int a, {required String name, double op = 1}) {
  return a;
}
