void main(List<String> args) {
  final p = const Person("111");
  final p1 = const Person("111");

  print(identical(p, p1));

  double pp = 12.4;
  print(pp);

  dynamic opt = double.parse("11.1");

  print(opt);
}

class Person {
  final String name;
  const Person(this.name);
}
