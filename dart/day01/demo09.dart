void main(List<String> args) {
  final names = ['zhangsan', 'lisi', 'wangwu'];

  for (var name in names) {
    print(name);
  }

  final per = Person.withAge('zhangsan', 12);
  print(per);

  final pMap = Person.withMap({});

  print(pMap);
}

class Person {
  String name;
  int age;

  Person(this.name, this.age);
  Person.withAge(this.name, this.age);
  Person.withMap(Map<String, dynamic> map)
      : name = map['name'] ?? "",
        age = map['age'] ?? 0;
  @override
  String toString() {
    // TODO: implement toString
    return "${name}-${age}";
  }
}
