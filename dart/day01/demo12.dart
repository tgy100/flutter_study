import 'demo07.dart';

void main(List<String> args) {
  final stu = Student('zhangli', 21, 10.1);
  final per = Person.withNameAndAge("wangwu", 15);
  print(stu);
  print(per);
}

class Person {
  String name;
  int age;
  Person(String name, int age)
      : this.name = name,
        this.age = age;

  Person.withNameAndAge(String name, int age) : this(name, age);

  @override
  String toString() {
    // TODO: implement toString
    return "${name}-${age}";
  }
}

class Student extends Person {
  double score;

  Student(String name, int age, double score)
      : this.score = score,
        super(name, age);

  @override
  String toString() {
    return "${super.name}-${score}-${super.age}";
  }
}
