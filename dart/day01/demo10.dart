void main(List<String> args) {
  final per = const Person(name: "111", age: 21);
  print(per);
  final per01 = const Person(name: "111", age: 21);

  print(identical(per, per01));

  final animal01 = const Animal('1111');
  print(animal01);
  final animal02 = const Animal('1111');
  print(animal02);

  print(identical(animal01, animal02));
}

class Person {
  final String name;
  final int age;

  const Person({required this.name, required this.age});

  @override
  String toString() {
    return "${name},${age}";
  }
}

class Animal {
  final String name;
  const Animal(this.name);

  @override
  String toString() {
    // TODO: implement toString
    return "${name}";
  }
}
