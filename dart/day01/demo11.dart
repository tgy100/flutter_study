void main(List<String> args) {
  final p1 = Person('1');
  final p2 = Person('1');
  print(identical(p1, p2));

  print(p1.getName);
  p1.setName = '里斯';
  print(p1.getName);
}

class Person {
  String name;
  static final Map<String, Person> personMap = {};

  // ignore: unused_element
  Person._internal(this.name);

  factory Person(key) {
    if (personMap.containsKey(key)) {
      return personMap[key]!;
    }

    final per = Person._internal("1212");
    personMap[key] = per;
    return per;
  }

  get getName {
    return name;
  }

  set setName(name) {
    this.name = name;
  }
}
