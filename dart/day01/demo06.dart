void main(List<String> args) {
  wrapper((int a, int b) => a + b);
  wrapper(sub);
}

typedef Callback = int Function(int, int);

void wrapper(Callback callback) {
  final a = 10;
  final b = 20;
  final result = callback(a, b);
  print(result);
}

int sub(int a, int b) {
  return a - b;
}
