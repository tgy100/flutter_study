void main(List<String> args) {
  final ints = [3, 2, 4, 5, 1];
  ints.sort();
  print("ints:${ints},${ints.runtimeType}");

  final sets = {1, 3, 2, 4};
  print(sets);

  final persons = {'name': 'zhangsan', 'age': 21};
  print(persons);
  persons.keys.forEach((element) {
    print("key:${element},value:${persons[element]}");
  });
}
