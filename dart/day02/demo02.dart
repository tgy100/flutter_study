void main(List<String> args) {
  Animal animal = Animal.dog();

  animal.run();
}

abstract class Animal {
  void run();

  factory Animal.dog() {
    return Dog("狗");
  }

  void test() {
    print("1111");
  }
}

class Dog implements Animal {
  String name;

  Dog(this.name);

  @override
  void run() {
    // TODO: implement run
    List.empty();
    print("dog run");
  }

  void test() {
    print("1111");
  }

  @override
  String toString() {
    // TODO: implement toString
    return "name:${name}";
  }
}
