void main(List<String> args) {
  final animal = Animal();

  animal.eat();
  animal.run();
}

mixin Action {
  void run();

  void eat() {
    print("1111");
  }
}

mixin Opt {
  void go() {
    print("go");
  }

  void move();
}

class Animal with Action, Opt {
  void run() {
    print("Animal run");
  }

  void move() {
    print("animal move");
  }
}
