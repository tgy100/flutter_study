void main(List<String> args) {
  final stu = Student("zhangsan", 21, 98);
  print(stu);
}

class Person {
  String name;
  int age;

  // dart 没有方法名重载，不能写相同的方法名
  Person() : this._init("", 0);
  Person._init(this.name, this.age);

  @override
  String toString() {
    // TODO: implement toString
    return "name:$name,age:$age";
  }
}

class Student extends Person {
  double score;

  Student(String name, int age, double score)
      : this.score = score,
        super._init(name, age);

  @override
  String toString() {
    // TODO: implement toString
    return "${super.toString()},score:${score}";
  }
}
