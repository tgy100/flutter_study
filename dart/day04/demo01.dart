import 'dart:io';

void main(List<String> args) {
  general(-2).then((value) {
    print(value);
  }).catchError((error) {
    print('catchError:$error');
  }).whenComplete(() => {print("whenComplete")});
}

Future<String> general(int second) {
  return Future(() {
    if (second <= 0) {
      throw "${second} param error";
    }

    sleep(Duration(seconds: second));
    return "asdasd";
  });
}
