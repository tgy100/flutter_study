import 'dart:io';

void main(List<String> args) {
  Future<String>(() {
    sleep(Duration(seconds: 2));
    return "睡眠完成";
  }).then((value) {
    print("$value");
    return Future.value("then 完成");
  }).then((value) {
    print(value);
  }).catchError((error) {
    print(error);
  }).whenComplete(() {
    print("whenComplete");
  });
}
